#!/bin/sh

usage() {
  echo "./predict.sh stock ::: predict the change of stock"
}

if [ $# -ne 1 ]; then
  usage
  exit 1
fi

./runpredict.sh /opt/matlab $1

