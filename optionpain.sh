#!/bin/sh

usage() {
  echo "./optionpain stock ::: get max pain of stock"
  echo "./optionpain stock [month [year]] ::: get max pain of stock at month year"
  echo "./optionpain all month year ::: get pain of all, and store in option/year-month"
  echo "./optionpain current stock [nodl] ::: get current pain of stock againt base"
  echo "./optionpain current all [nodl] ::: get current pain of all against base"
  echo "     nodl means no download again, default (empty) is redownload."
  echo "./optionpain stat ::: get pain stat against base, and store in optionstat.txt"
}

mpain() {
  stock=$(echo $1 | tr [:lower:] [:upper:])
  python code/maxpain.py $@
  if [ $? -ne 0 ]; then
    echo $stock
    return 1
  fi
  rm -f option/$stock.opt
}

cpain() {
  stock=$(echo $1 | tr [:lower:] [:upper:])
  if [ -z "$2" ] ; then
    mpain $stock
  fi
  python code/currentpain.py $stock
  rm -f option/$stock.base
  ln -s base/$stock.max option/$stock.base
}

if [ $# -eq 0 ]; then
  usage
  exit 1
fi

if [ $1 = "current" ] || [ $1 = "curr" ] ; then
  if [ $# -ne 2 ] && [ $# -ne 3 ] ; then
    usage
    exit 1
  fi
  if [ $2 = "all" ]; then
  #./optionpain current all
    cat stock.list | while read stock ; do
      if [ "$stock" = "" ]; then
        continue
      fi
      cpain $stock $3
    done
  else
  # ./optionpain current stock
    stock=$2
    cpain $stock $3
    cat option/$stock.max | grep ratio
    cat option/$stock.curr | grep ratio
    cat option/$stock.base | grep ratio
  fi
elif [ $1 = "stat" ]; then
  if [ $# -ne 1 ]; then
    usage
    exit 1
  fi
  # ./optionpain stat
  ./optionpain.sh curr all nodl
  echo "option, price, currpain, |cp-p|/p, curr_p/c, cp_p/c, currvol, maxpain, |mp-p|/p, max_p/c, mp_p/c, maxvol" > optionstat.txt
  cat stock.list | while read stock ; do
    if [ "$stock" = "" ]; then
      continue
    fi
    ./optionstat $stock >> optionstat.txt
  done
else
  if [ $1 = "all" ]; then
    if [ $# -ne 3 ]; then
      usage
      exit 1
    fi
    # ./optionpain all month year
    cp -f stock.list.daily stock.list
    year=$3
    month=$2
    folder=option/$year-$month
    rm -rf $folder
    mkdir $folder
    cat stock.list | while read stock ; do
      if [ "$stock" = "" ]; then
        continue
      fi
      echo $stock $month $year
      mpain $stock $month $year
      mv option/$stock.max $folder
    done
    cd option
    rm -rf base
    ln -s $year-$month base
    cd ..
  else
    stock=$(echo $1 | tr [:lower:] [:upper:])
    mpain $@
    cat option/$stock.max | grep ratio
  fi
fi

exit 0
