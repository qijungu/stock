function div = detectstock(ts, offset, window)
% detect(stock, offset, window)
% offset : 0, today, -1, yesterday, -2, the day before yesterday
% window : window for detecting signal
% return the divergence and sentiment of the past duration days, inclding today

for i = [1 : window]
    div = stocksignal(ts(end + offset - i + 1 - 126 : end + offset - i + 1));
    sbulldiv(i) = max(div(1), 0);
    sbeardiv(i) = max(-div(1), 0);
    lbulldiv(i) = max(div(2), 0) * 2;
    lbeardiv(i) = max(-div(2), 0) * 2;
    mpricediv(i) = div(3);
end;
sbulldiv = sbulldiv(window : -1 : 1)';
sbeardiv = sbeardiv(window : -1 : 1)';
lbulldiv = lbulldiv(window : -1 : 1)';
lbeardiv = lbeardiv(window : -1 : 1)';
mpricediv = mpricediv(window : -1 : 1)';

sti = sentiment(ts(end + offset - 20 : end + offset), 10);
sti = getfield(sti(end - window + 1 : end), 'SENTI');

div = fints(ts.dates(end + offset - window + 1 : end + offset), [sbulldiv, sbeardiv, lbulldiv, lbeardiv, sti, mpricediv], {'STBE', 'STBU', 'LTBE', 'LTBU', 'SENTI', 'PDIV'});
