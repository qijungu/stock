function [TH, TL] = trueHL(ts)
% truehigh: TH = max(high_today, close_yesterday)
% truelow : TL = min(low_today, close_yesterday

tslen = length(ts);
high = getfield(ts, 'HIGH');
low = getfield(ts, 'LOW');
close = getfield(ts, 'CLOSE');
closey = [close(1) ; close(1 : tslen - 1)]; % offset by one to get yesterday's close

TH = (max([high'; closey']))';
TL = (min([low'; closey']))';