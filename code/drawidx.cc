#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    char index[10];
    char cmd[1000];
    if (argc == 1) {
        printf("./drawidx all [startday [endday]]\n");
        printf("./drawidx index [startday [endday]]\n");
        printf("\n");
        return 1;
    }
    int startday = 252;
    int endday = 0;
    if (argc >= 3) startday = atoi(argv[2]);
    if (argc >= 4) endday = atoi(argv[3]);
    if (strncmp(argv[1], "all", 3) == 0) {
        printf("Drawing all indices\n\n");
        FILE* fp = fopen("index.list", "r");
        while(fscanf(fp, "%s", index)) {
            if (feof(fp)) break; // reach the end of index list
            if (index[0] == '#') continue; // comment out
            printf("Drawing %s\n", index);
            //sprintf(cmd, "./rundrawindex.sh $MATLAB %s 1 %d %d", index, startday, endday);
            sprintf(cmd, "matlab 1>/dev/null -nosplash -nodesktop -r \"drawindex(\'%s\', \'1\', \'%d\', \'%d\'); exit\"", index, startday, endday);
            if (system(cmd) < 0) break;
        }
        fclose(fp);
    } else {
        strncpy(index, argv[1], 10);
        //sprintf(cmd, "./rundrawindex.sh $MATLAB %s 0 %d %d", index, startday, endday);
        sprintf(cmd, "matlab 1>/dev/null -nosplash -nodesktop -r \"drawindex(\'%s\', \'1\', \'%d\', \'%d\'); exit\"", index, startday, endday);
        system(cmd);
    }
    printf("\n");
    return 0;
}
