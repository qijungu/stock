function div = stocksignal(ts)

sduration = 21;
mduration = 42;
lduration = 126;

tt = truetrend(ts);
tt = normalizets(tt.TRTDVOL);
sttang = atan(coef(tt(end - sduration + 1: end)));
% mttang = atan(coef(tt(end - mduration + 1: end)));
lttang = atan(coef(tt(end - lduration + 1: end)));

sti = sentiment(ts, 10);
sstiang = atan(coef(sti(end - sduration + 1: end)));
% lstiang = atan(coef(sti(end - lduration + 1: end)));

pc = normalizets(ts.CLOSE);
spcang = atan(coef(pc(end - sduration + 1: end)));

% div = [sstiang - sttang, lstiang - lttang, mpcang - mttang];
div = [sstiang - sttang, 0 - lttang, spcang - sttang];

% tt = truetrend(ts);
% tt = normalizets(tt.TRTDVOL);
% sti = sentiment(ts, 10);
% pc = normalizets(ts.CLOSE);
% 
% sdelta = delta(sti(end - sduration + 1 : end), tt(end - sduration + 1 : end));
% ldelta = delta(sti(end - lduration + 1 : end), tt(end - lduration + 1 : end));
% pdelta = delta(pc(end - mduration + 1: end), tt(end - mduration + 1: end));
% div = [sdelta, ldelta, pdelta] / 20;

function k = coef(ts)
tsmat = fts2mat(ts, 1);
k = regress(tsmat(:, 2), [tsmat(:, 1), ones(length(ts), 1)]);
k = k(1);

function d = delta(ts1, ts2)
len = length(ts1);
tsmat1 = fts2mat(ts1, 0);
tsmat1 = tsmat1 - mean(tsmat1);
tsmat2 = fts2mat(ts2, 0);
tsmat2 = tsmat2 - mean(tsmat2);
d = std(tsmat1 - tsmat2);
