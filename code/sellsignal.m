function [sell, measure] = sellsignal(div)
% div is a ts of divergence signaL
% return true if sell

len = length(div);
date = getfield(div, 'dates');
stbe = getfield(div, 'STBE');
stbek = regress(stbe, [date, ones(len, 1)]);
stbek = stbek(1);
stbu = getfield(div, 'STBU');
stbuk = regress(stbu, [date, ones(len, 1)]);
stbuk = stbuk(1);
% mtbe = getfield(div, 'MTBE');
% mtbek = regress(mtbe, [date, ones(len, 1)]);
% mtbek = mtbek(1);
% mtbu = getfield(div, 'MTBU');
% mtbuk = regress(mtbu, [date, ones(len, 1)]);
% mtbuk = mtbuk(1);
ltbe = getfield(div, 'LTBE');
ltbek = regress(ltbe, [date, ones(len, 1)]);
ltbek = ltbek(1);
ltbu = getfield(div, 'LTBU');
ltbuk = regress(ltbu, [date, ones(len, 1)]);
ltbuk = ltbuk(1);
sti = getfield(div, 'SENTI');
stik = regress(sti, [date, ones(len, 1)]);
stik = stik(1);
pdiv = getfield(div, 'PDIV');
pdivk = regress(pdiv, [date, ones(len, 1)]);
pdivk = pdivk(1);

% pdiv tops above 0.013
if peak(pdiv) && max(pdiv) > 0.013
    sell = 5;
    
% stik peaks above 0.8
elseif peak(sti) && max(sti) > 0.8
    sell = 4.5;
    
else
    sell = 0;
end;

measure = [max(stbe), max(stbu), max(ltbe), max(ltbu), max(sti)];
