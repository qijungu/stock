function p = peak(x)

len = length(x);
    
if (x(len - 2) < x (len -1) && x(len - 1) >= x(len)) || (x(len - 3) < x (len -2) && x(len - 2) >= x(len - 1))
    p = 1;
else
    p = 0;
end;
