function WADV = willadvol(ts)
% 1. Calculate the True Range High and True Range Low:
% 2. Compare Closing price to yesterday's Closing price:
%     * If Close [today] is greater than Close [yesterday]
%                  Price Move [today] = Close [today] - True Range Low
%     * If Close [today] is less than Close [yesterday]
%                  Price Move [today] = Close [today] - True Range High
%     * If Close [today] equals Close [yesterday]
%                  Price Move [today] = zero
% 3. Multiply the price move by volume:
%                       AD [today] = Price Move [today] * Volume [today]
% 4. Calculate the cumulative total:
%                       Williams AD = AD [today] + Williams AD [yesterday]

[TH, TL] = trueHL(ts); % step 1
tslen = length(ts);
date = getfield(ts, 'dates');
close = getfield(ts, 'CLOSE');
vol = getfield(ts, 'VOLUME');
wad(1) = 0;
for i = [2 : tslen]
%     if close(i) > close(i - 1) % step 2
%         pm = close(i) - TL(i);
%     elseif close(i) < close(i - 1)
%         pm = close(i) - TH(i);
%     else
%         pm = 0;
%     end;
    netchange = close(i) - close(i - 1);
    truerange = TH(i) - TL(i);
    if truerange == 0
        ad = vol(i);
    else
        ad = netchange / truerange * vol(i); % step 3
    end
    ac(i) = 0;
    ds(i) = 0;
    if ad > 0
        ac(i) = ad;
    elseif ad < 0
        ds(i) = -ad;
    end;
    wad(i) = ad + wad(i - 1);
end;

WADV = fints(date, [wad; ac; ds]', {'WADVOL', 'ACCUM', 'DISTR'});