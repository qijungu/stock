#!/usr/bin/python
# -*- coding: utf-8 -*-

from datetime import date
import sys
import pyetrade

oauth = pyetrade.ETradeOAuth("qijungu", "dg754001@")
oauth.get_request_token()
#Follow url and get verification code
tokens = oauth.get_access_token(verifier_code)
accounts = pyetrade.ETradeAccounts(
        "qijungu",
        "dg754001@", 
        tokens['oauth_token'],
        tokens['oauth_token_secret']
    )
accounts.list_accounts()

# MON=['Jan','Fed','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
# 
# # return the date of the third Friday
# def oedate(month, year) :
#     
#     fcnt = 0
#     for i in range(1, 26) :
#         oe = date(year, month, i)
#         if oe.weekday() == 4 :
#             fcnt = fcnt+1
#             if fcnt == 3 :
#                 break
#     return i
# 
# def saveoptions(options, stock):
# 
#     # get call and put chains
#     calls = []
#     puts = []
#     for key in options:
#         optval = {}
#         optval['strike'] = float(key[0])
#         optval['symbol'] = options[key].symbol
#         optval['last'] = -1.0
#         optval['bid'] = -1.0
#         optval['ask'] = -1.0
#         optval['vol'] = 0
#         optval['openint'] = 0
#         if key[1] == 'call':
#             optval['type'] = 'call'
#             calls.append(optval)
#         else:
#             optval['type'] = 'put'
#             puts.append(optval)
#     
#     calls.sort(key=lambda opt: opt['strike'])
#     puts.sort(key=lambda opt: opt['strike'])
#     
#     symbols = {}
#     # get calls
#     for call in calls:
#         symbols[call['symbol']] = TradeMonsterConstants.INSTRUMENTS.OPTION
#     cvals = quotesService.getParsedQuotes(symbols)
#     for symbol in cvals:
#         cval = cvals[symbol]
#         for call in calls:
#             if call['symbol'] == symbol:
#                 call['last'] = float(cval.lastPrice)
#                 call['bid'] = float(cval.bidPrice)
#                 call['ask'] = float(cval.askPrice)
#                 call['vol'] = int(float(cval.volume))
#                 call['openint'] = int(float(cval.openInterest))
#     # get puts
#     for put in puts:
#         symbols[put['symbol']] = TradeMonsterConstants.INSTRUMENTS.OPTION
#     pvals = quotesService.getParsedQuotes(symbols)
#     for symbol in pvals:
#         pval = pvals[symbol]
#         for put in puts:
#             if put['symbol'] == symbol:
#                 put['last'] = float(pval.lastPrice)
#                 put['bid'] = float(pval.bidPrice)
#                 put['ask'] = float(pval.askPrice)
#                 put['vol'] = int(float(pval.volume))
#                 put['openint'] = int(float(pval.openInterest))
# 
#     calltmp = []
#     puttmp = []
#     ic = 0
#     ip = 0
#     while ic < len(calls) or ip < len(puts) :
#         opr = {}
#         if (ic == len(calls) and ip < len(puts)) or (ic < len(calls) and ip < len(puts) and calls[ic]['strike'] > puts[ip]['strike']) :
#             # need dummy call
#             opr['type'] = 'call'
#             opr['strike'] = puts[ip]['strike']
#             opr['last'] = -1.0
#             opr['bid'] = -1.0
#             opr['ask'] = -1.0
#             opr['vol'] = 0
#             opr['openint'] = 0
#             calltmp.append(opr)
#             puttmp.append(puts[ip])
#             ip += 1
#         elif (ip == len(puts) and ic < len(calls)) or (ic < len(calls) and ip < len(puts) and calls[ic]['strike'] < puts[ip]['strike']) :
#             # need dummy put
#             opr['type'] = 'put'
#             opr['strike'] = calls[ic]['strike']
#             opr['last'] = -1.0
#             opr['bid'] = -1.0
#             opr['ask'] = -1.0
#             opr['vol'] = 0
#             opr['openint'] = 0
#             puttmp.append(opr)
#             calltmp.append(calls[ic])
#             ic += 1
#         else :
#             # equal
#             assert(calls[ic]['strike'] == puts[ip]['strike'])
#             calltmp.append(calls[ic])
#             puttmp.append(puts[ip])
#             ic += 1
#             ip += 1
#     calls = calltmp
#     puts = puttmp
# 
#     optionlog = "option/%s.max" % stock
#     fpoptlog = open(optionlog, "w")
#     assert(fpoptlog)
#   
#     fpoptlog.write("                      Call                ,   %6s,                       Put                 \n" % stock)
#     fpoptlog.write("     Bid,      Ask,        Vol,   Open Int,         ,      Bid,      Ask,        Vol,   Open Int\n")
#     for i in range(0, len(calls)) :
#         assert(calls[i]["strike"] == puts[i]["strike"])
#         fpoptlog.write("% 8.2lf, % 8.2lf, % 10ld, % 10ld, % 8.2lf, % 8.2lf, % 8.2lf, % 10ld, % 10ld\n" % (
#                            calls[i]["bid"], calls[i]["ask"], calls[i]["vol"], calls[i]["openint"], 
#                            calls[i]["strike"], 
#                            puts[i]["bid"], puts[i]["ask"], puts[i]["vol"], puts[i]["openint"]
#                            )
#                        )
#   
#     maxstrike = -1.0
#     maxpain = -1.0
#     strike = calls[0]["strike"]
#     while strike <= calls[-1]["strike"] :
#         pain = 0.0
#         for i in range(0, len(calls)) :
#             if calls[i]["strike"] <= strike :
#                 pain += (strike - calls[i]["strike"]) * calls[i]["openint"]
#             if puts[i]["strike"] >= strike :
#                 pain += (puts[i]["strike"] - strike) * puts[i]["openint"]
#         if maxpain < 0 or pain < maxpain :
#             maxpain = pain
#             maxstrike = strike
#         strike += 0.25
#   
#     ccnt = 0.0 # simply count open interest
#     pcnt = 0.0
#     ceffect = 0.0 # count open interest with price
#     peffect = 0.0
#     cprice = 0.0 # count open interest multiply price
#     pprice = 0.0
#     for i in range(0, len(calls)) :
#         ccnt += calls[i]["openint"]
#         pcnt += puts[i]["openint"]
#         if calls[i]["bid"] > 0.03 and calls[i]["ask"] > 0.03 :
#             ceffect += calls[i]["openint"]
#             cprice += calls[i]["openint"] * (calls[i]["bid"] + calls[i]["ask"]) / 2
#         if puts[i]["bid"] > 0.03 and puts[i]["ask"] > 0.03 :
#             peffect += puts[i]["openint"]
#             pprice += puts[i]["openint"] * (puts[i]["bid"] + puts[i]["ask"]) / 2
#     
#     if ccnt == 0 :
#         ratio = -1
#     else :                
#         ratio = pcnt / ccnt
#     if ceffect == 0 :
#         ratioeffect = -1
#     else :
#         ratioeffect = peffect / ceffect
#     if cprice == 0 :
#         ratioprice = -1
#     else :
#         ratioprice = pprice / cprice
#   
#     fpoptlog.write("====== max pain is at %0.2lf, priced p/c ratio %0.2lf ======\n" % (maxstrike, ratioprice))
#     fpoptlog.write("p/c ratio is %0.2lf, put %0.0lf, call %0.0lf, total %0.0lf\n" % (ratio, pcnt, ccnt, pcnt + ccnt))
#     fpoptlog.write("P/C ratio is %0.2lf, put %0.0lf, call %0.0lf, total %0.0lf (with effective price)\n" % (ratioeffect, peffect, ceffect, peffect + ceffect))
#   
#     fpoptlog.close()
#  
#     return
# 
# def main():
#     
#     if len(sys.argv) < 2 or len(sys.argv) > 5 :
#         print 'python maxpain.py stock [month [year]]'
#         print 'default is this month, this year'
#         return
# 
#     timenow = date.today()
#     yearnow = timenow.year
#     monthnow = timenow.month
#     daynow = timenow.day
# 
#     stock = sys.argv[1].upper()
#     year = yearnow
#     month = monthnow
# 
#     if len(sys.argv) == 3 :
#         month = int(sys.argv[2])
#         if month < monthnow :
#             year = yearnow + 1
# 
#     if len(sys.argv) == 4 :
#         month = int(sys.argv[2])
#         year = int(sys.argv[3])
# 
#     day = oedate(month, year)
#     
#     if day < daynow and month == monthnow :
#         month = monthnow % 12 + 1
#         if month < monthnow :
#             year = yearnow + 1
# 
#     month = MON[month-1]
#     year = '%02d' % (year % 100)
# 
#     results = quotesService.getParsedOptionChain(stock)
# 
#     for i in range(0,len(results)):
#         if results[i].expiryLabel == unicode(month+year) and results[i].expiryType == u'Standard':
#             saveoptions(results[i].options, stock)
# 
# main()
#                 
