function [volhist, volbins] = volpricehist(ts, binnum)
vol = getfield(ts, 'VOLUME');
price = getfield(ts, 'CLOSE');
mprice = min(price);
Mprice = max(price);
bin = (Mprice - mprice) / binnum;
len = length(ts);

volhist = zeros(1, binnum);
volbins = zeros(1, binnum);
for i = [1 : binnum]
    volbins(i) = mprice + (i - 0.5) * bin;
end;

for i = [1 : len]
    binidx = min([floor((price(i) - mprice) / bin) + 1, binnum]);
    volhist(binidx) = volhist(binidx) + vol(i);
end;
