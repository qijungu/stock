function drawchart(varargin)
% drawchar(stock, backend, startday, endday)
% backend: 1 no show figure, 0 show figure
% startday: the start day of the chart. -126 or 126 means 126 stock days before today
% endday: the end day of the chart. -1 or 1 means 1 stock day before today
% default: stock = 'SPY', backend = 0, startday = 126 or -126, endday = 0;

netfile3 = {}; %201712
netfile4 = {'ml/mlpr_xiv_sol4_0.024927.mat','ml/mlpr_xiv_sol4_0.025419.mat','ml/mlpr_xiv_sol4_0.028080.mat','ml/mlpr_xiv_sol4_0.028773.mat'}; %201712
netfile5 = {}; %201712
netfile6 = {}; %201712
netfile6 = {}; %201712
numfeature3 = 15; % number of features
numfeature4 = 17; % number of features
numfeature5 = 30; % number of features
numfeature6 = 32; % number of features

stock = upper('xiv');
backend = 0;
startday = 252; % 126, half year, 252, 1 year
endday = 0;

argsz = length(varargin);
if argsz >= 1
    stock = upper(varargin{1});
end;
if argsz >= 2
    backend = str2num(varargin{2});
end;
if argsz >= 3
    startday = abs(str2num(varargin{3}));
end;
if argsz >= 4
    endday = abs(str2num(varargin{4}));
end;

stockdata = ['data/matlab/', stock, '.dat'];
sdts = ascii2fts(stockdata, 1, 2, []);
figdata = ['fig/data/', stock, '.dat'];
fdts = ascii2fts(figdata, 1, 2, []);

% startday = min([startday, length(fdts) - 126]);
startday = min([startday, length(fdts)]);
if endday >= startday - 5
    fprintf('The startday %d must be at least 5 days before the endday %d\n', startday, endday);
    return;
end;

sdts = sdts(end - startday : end - endday);
fdts = fdts(end - startday : end - endday);

date = getfield(sdts, 'dates');
len = length(sdts);

[pvolhist, pvolbins] = volpricehist(sdts(end - min(126, len) + 1 : end), 10);
pvolhist = pvolhist / max(pvolhist);

[LCON10, MLCON10] = lowconcave(sdts.LOW(end - 10 : end)); % two weeks
[LCON20, MLCON20] = lowconcave(sdts.LOW(end - 20 : end)); % one month
[LCON60, MLCON60] = lowconcave(sdts.LOW(end - 60 : end)); % three months

[HCON10, MHCON10] = highconcave(sdts.HIGH(end - 10 : end)); % two weeks
[HCON20, MHCON20] = highconcave(sdts.HIGH(end - 20 : end)); % one month
[HCON60, MHCON60] = highconcave(sdts.HIGH(end - 60 : end)); % three months

pCLOSE = sdts.CLOSE;
pBOLL = [fdts.MIDDLE, fdts.UPPER, fdts.LOWER];
pMOV = [fdts.MOV200, fdts.MOV100, fdts.MOV50];
BOLLWINDOW = pCLOSE([end-20, end]);

buy = fts2mat(fdts.BUY);
sell = fts2mat(fdts.SELL);
idx = find(buy);
if isempty(idx)
    buy = [];
else
    buy = fints(fdts.dates(idx), buy(idx), 'BUY');
end;
idx = find(sell);
if isempty(idx) 
    sell = [];
else
    sell = fints(fdts.dates(idx), sell(idx), 'SELL');
end;

sduration = 21;
mduration = 42;
lduration = 126;

trtdsmat = fts2mat(fdts.TRTDVOL(end - sduration + 1 : end), 1);
trtdscoef = regress(trtdsmat(:, 2), [trtdsmat(:, 1), ones(sduration, 1)]);
trtds = [trtdsmat(1, 1), [trtdsmat(1, 1), 1] * trtdscoef; trtdsmat(sduration, 1), [trtdsmat(sduration, 1), 1] * trtdscoef];
trtdmmat = fts2mat(fdts.TRTDVOL(end - mduration + 1 : end), 1);
trtdmcoef = regress(trtdmmat(:, 2), [trtdmmat(:, 1), ones(mduration, 1)]);
trtdm = [trtdmmat(1, 1), [trtdmmat(1, 1), 1] * trtdmcoef; trtdmmat(mduration, 1), [trtdmmat(mduration, 1), 1] * trtdmcoef];
trtdlmat = fts2mat(fdts.TRTDVOL(end - lduration + 1 : end), 1);
trtdlcoef = regress(trtdlmat(:, 2), [trtdlmat(:, 1), ones(lduration, 1)]);
trtdl = [trtdlmat(1, 1), [trtdlmat(1, 1), 1] * trtdlcoef; trtdlmat(lduration, 1), [trtdlmat(lduration, 1), 1] * trtdlcoef];

wadsmat = fts2mat(fdts.WADVOL(end - sduration + 1 : end), 1);
wadscoef = regress(wadsmat(:, 2), [wadsmat(:, 1), ones(sduration, 1)]);
wads = [wadsmat(1, 1), [wadsmat(1, 1), 1] * wadscoef; wadsmat(sduration, 1), [wadsmat(sduration, 1), 1] * wadscoef];
wadlmat = fts2mat(fdts.WADVOL(end - lduration + 1 : end), 1);
wadlcoef = regress(wadlmat(:, 2), [wadlmat(:, 1), ones(lduration, 1)]);
wadl = [wadlmat(1, 1), [wadlmat(1, 1), 1] * wadlcoef; wadlmat(lduration, 1), [wadlmat(lduration, 1), 1] * wadlcoef];

stismat = fts2mat(fdts.SENTI(end - sduration + 1 : end), 1);
stiscoef = regress(stismat(:, 2), [stismat(:, 1), ones(sduration, 1)]);
stis = [stismat(1, 1), [stismat(1, 1), 1] * stiscoef; stismat(sduration, 1), [stismat(sduration, 1), 1] * stiscoef];
stilmat = fts2mat(fdts.SENTI(end - lduration + 1 : end), 1);
stilcoef = regress(stilmat(:, 2), [stilmat(:, 1), ones(lduration, 1)]);
stil = [stilmat(1, 1), [stilmat(1, 1), 1] * stilcoef; stilmat(lduration, 1), [stilmat(lduration, 1), 1] * stilcoef];

pricemmat = fts2mat(sdts.CLOSE(end - mduration + 1 : end), 1);
pricemcoef = regress(pricemmat(:, 2), [pricemmat(:, 1), ones(mduration, 1)]);
pricem = [pricemmat(1, 1), [pricemmat(1, 1), 1] * pricemcoef; pricemmat(mduration, 1), [pricemmat(mduration, 1), 1] * pricemcoef];

if strcmp(stock, 'XIV')
    swindow = 4; % short window to get vix change rate
    lwindow = 8; % long window to get vix change rate
    delay = 60; % delay
    numfeature = 13; % number of features

    tsxiv = ascii2fts('data/matlab/XIV.dat', 1, 2, []);
    tsxiv = tsxiv(end - startday - delay*2 : end - endday);
    tsspy = ascii2fts('data/matlab/SPY.dat', 1, 2, []);
    tsspy = tsspy(end - startday - delay*2 : end - endday);
    tsvix = ascii2fts('data/matlab/VIX.dat', 1, 2, []);
    tsvix = tsvix(end - startday - delay*2 : end - endday);

    % p: price, v: volume, c: change, b: bolling, rs: ret short, rl: ret long
    xivp = fts2mat(tsxiv, 0, 'CLOSE');
    xivh = fts2mat(tsxiv, 0, 'HIGH');
    xivl = fts2mat(tsxiv, 0, 'LOW');
    xivo = fts2mat(tsxiv, 0, 'OPEN');
    xivv = fts2mat(tsxiv, 0, 'VOLUME');
    xivc = xivp(2:end) ./ xivp(1:end-1) - 1;
    [xivmid, xivup, xivlow] = bollinger(tsxiv);
    xivb = fts2mat(xivup.CLOSE) - fts2mat(xivlow.CLOSE);
    xivup=fts2mat(xivup, 'CLOSE');
    xivlow=fts2mat(xivlow, 'CLOSE');
    xivrs = xivp(swindow+1:end)./xivp(swindow/2+1:end-swindow/2) + xivp(swindow/2+1:end-swindow/2)./xivp(1:end-swindow);
    xivrl = xivp(lwindow+1:end)./xivp(lwindow/2+1:end-lwindow/2) + xivp(lwindow/2+1:end-lwindow/2)./xivp(1:end-lwindow);
    spyp = fts2mat(tsspy, 0, 'CLOSE');
    spyh = fts2mat(tsspy, 0, 'HIGH');
    spyl = fts2mat(tsspy, 0, 'LOW');
    spyo = fts2mat(tsspy, 0, 'OPEN');
    spyv = fts2mat(tsspy, 0, 'VOLUME');
    spyc = spyp(2:end) ./ spyp(1:end-1) - 1;
    [spymid, spyup, spylow] = bollinger(tsspy);
    spyb = fts2mat(spyup.CLOSE) - fts2mat(spylow.CLOSE);
    spyup=fts2mat(spyup, 'CLOSE');
    spylow=fts2mat(spylow, 'CLOSE');
    spyrs = spyp(swindow+1:end)./spyp(swindow/2+1:end-swindow/2) + spyp(swindow/2+1:end-swindow/2)./spyp(1:end-swindow);
    spyrl = spyp(lwindow+1:end)./spyp(lwindow/2+1:end-lwindow/2) + spyp(lwindow/2+1:end-lwindow/2)./spyp(1:end-lwindow);
    [vixmid, vixup, vixlow] = bollinger(tsvix);
    vixb = fts2mat(vixup.CLOSE) - fts2mat(vixlow.CLOSE);
    vixp = fts2mat(tsvix, 0, 'CLOSE');
    vixh = fts2mat(tsvix, 0, 'HIGH');
    vixl = fts2mat(tsvix, 0, 'LOW');
    vixo = fts2mat(tsvix, 0, 'OPEN');
    vixc = vixp(2:end) ./ vixp(1:end-1) - 1;
    vixpn = (vixp - fts2mat(vixmid.CLOSE)) ./ vixb; % normalized vixp
    vixcn = vixpn(2:end) - vixpn(1:end-1); % normalized vixp change rate
    vixup=fts2mat(vixup, 'CLOSE');
    vixlow=fts2mat(vixlow, 'CLOSE');
    vixrs = vixp(swindow+1:end)./vixp(swindow/2+1:end-swindow/2) + vixp(swindow/2+1:end-swindow/2)./vixp(1:end-swindow);
    vixrl = vixp(lwindow+1:end)./vixp(lwindow/2+1:end-lwindow/2) + vixp(lwindow/2+1:end-lwindow/2)./vixp(1:end-lwindow);

    inputs3 = zeros(numfeature3*delay, len);
    for i=0:delay-1
        inputs3(i*numfeature3+1:(i+1)*numfeature3,:)=[
        xivp(end-i-len+1:end-i)';
        xivv(end-i-len+1:end-i)';
        xivc(end-i-len+1:end-i)';
        xivb(end-i-len+1:end-i)';
        spyp(end-i-len+1:end-i)';
        spyv(end-i-len+1:end-i)';
        spyc(end-i-len+1:end-i)';
        spyb(end-i-len+1:end-i)';
        vixp(end-i-len+1:end-i)';
        vixc(end-i-len+1:end-i)';
        vixb(end-i-len+1:end-i)';
        vixup(end-i-len+1:end-i)';
        vixlow(end-i-len+1:end-i)';
        vixrs(end-i-len+1:end-i)';
        vixrl(end-i-len+1:end-i)';
        ];
    end;

    tsnet3 = {};
    for i=1:length(netfile3)
        nf3 = netfile3{i};
        net3 = load(nf3);
        net3 = net3.net;
        outputs3 = net3(inputs3);
        a=tsxiv(end-len+1:end);
        tsnet3{i} = fints(a.dates, outputs3',{'RiseFall'});
    end;

    inputs4 = zeros(numfeature4*delay, len);
    for i=0:delay-1
        inputs4(i*numfeature4+1:(i+1)*numfeature4,:)=[
        xivp(end-i-len+1:end-i)';
        xivv(end-i-len+1:end-i)';
        xivc(end-i-len+1:end-i)';
        xivb(end-i-len+1:end-i)';
        spyp(end-i-len+1:end-i)';
        spyv(end-i-len+1:end-i)';
        spyc(end-i-len+1:end-i)';
        spyb(end-i-len+1:end-i)';
        vixp(end-i-len+1:end-i)';
        vixc(end-i-len+1:end-i)';
        vixpn(end-i-len+1:end-i)';
        vixcn(end-i-len+1:end-i)';
        vixb(end-i-len+1:end-i)';
        vixup(end-i-len+1:end-i)';
        vixlow(end-i-len+1:end-i)';
        vixrs(end-i-len+1:end-i)';
        vixrl(end-i-len+1:end-i)';
        ];
    end;

    tsnet4 = {};
    for i=1:length(netfile4)
        nf4 = netfile4{i};
        net4 = load(nf4);
        net4 = net4.net;
        outputs4 = net4(inputs4);
        a=tsxiv(end-len+1:end);
        tsnet4{i} = fints(a.dates, outputs4',{'RiseFall'});
    end;

    inputs5 = zeros(numfeature5*delay, len);
    for i=0:delay-1
        inputs5(i*numfeature5+1:(i+1)*numfeature5,:)=[
        xivp(end-i-len+1:end-i)';
        xivh(end-i-len+1:end-i)';
        xivl(end-i-len+1:end-i)';
        xivo(end-i-len+1:end-i)';
        xivv(end-i-len+1:end-i)';
        xivb(end-i-len+1:end-i)';
        xivup(end-i-len+1:end-i)';
        xivlow(end-i-len+1:end-i)';
        xivc(end-i-len+1:end-i)';
        spyp(end-i-len+1:end-i)';
        spyh(end-i-len+1:end-i)';
        spyl(end-i-len+1:end-i)';
        spyo(end-i-len+1:end-i)';
        spyv(end-i-len+1:end-i)';
        spyc(end-i-len+1:end-i)';
        spyb(end-i-len+1:end-i)';
        spyup(end-i-len+1:end-i)';
        spylow(end-i-len+1:end-i)';
        spyrs(end-i-len+1:end-i)';
        spyrl(end-i-len+1:end-i)';
        vixp(end-i-len+1:end-i)';
        vixh(end-i-len+1:end-i)';
        vixl(end-i-len+1:end-i)';
        vixo(end-i-len+1:end-i)';
        vixc(end-i-len+1:end-i)';
        vixb(end-i-len+1:end-i)';
        vixup(end-i-len+1:end-i)';
        vixlow(end-i-len+1:end-i)';
        vixrs(end-i-len+1:end-i)';
        vixrl(end-i-len+1:end-i)';
        ];
    end;

    tsnet5 = {};
    for i=1:length(netfile5)
        nf5 = netfile5{i};
        net5 = load(nf5);
        net5 = net5.net;
        outputs5 = net5(inputs5);
        a=tsxiv(end-len+1:end);
        tsnet5{i} = fints(a.dates, outputs5',{'RiseFall'});
    end;

    inputs6 = zeros(numfeature6*delay, len);
    for i=0:delay-1
        inputs6(i*numfeature6+1:(i+1)*numfeature6,:)=[
        xivp(end-i-len+1:end-i)';
        xivh(end-i-len+1:end-i)';
        xivl(end-i-len+1:end-i)';
        xivo(end-i-len+1:end-i)';
        xivv(end-i-len+1:end-i)';
        xivb(end-i-len+1:end-i)';
        xivup(end-i-len+1:end-i)';
        xivlow(end-i-len+1:end-i)';
        xivc(end-i-len+1:end-i)';
        spyp(end-i-len+1:end-i)';
        spyh(end-i-len+1:end-i)';
        spyl(end-i-len+1:end-i)';
        spyo(end-i-len+1:end-i)';
        spyv(end-i-len+1:end-i)';
        spyc(end-i-len+1:end-i)';
        spyb(end-i-len+1:end-i)';
        spyup(end-i-len+1:end-i)';
        spylow(end-i-len+1:end-i)';
        spyrs(end-i-len+1:end-i)';
        spyrl(end-i-len+1:end-i)';
        vixp(end-i-len+1:end-i)';
        vixh(end-i-len+1:end-i)';
        vixl(end-i-len+1:end-i)';
        vixo(end-i-len+1:end-i)';
        vixc(end-i-len+1:end-i)';
        vixpn(end-i-len+1:end-i)';
        vixcn(end-i-len+1:end-i)';
        vixb(end-i-len+1:end-i)';
        vixup(end-i-len+1:end-i)';
        vixlow(end-i-len+1:end-i)';
        vixrs(end-i-len+1:end-i)';
        vixrl(end-i-len+1:end-i)';
        ];
    end;

    tsnet6 = {};
    for i=1:length(netfile6)
        nf6 = netfile6{i};
        net6 = load(nf6);
        net6 = net6.net;
        outputs6 = net6(inputs6);
        a=tsxiv(end-len+1:end);
        tsnet6{i} = fints(a.dates, outputs6',{'RiseFall'});
    end;
end;

figure(1); close(1);
fh =figure(1);
if backend
    set(fh, 'Visible', 'off');
end;

subplot(2, 1, 1);
hold on;
h = candle(sdts);
yl = ylim;
set(get(get(h(1),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(2),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(3),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = barh(pvolbins, pvolhist * (datenum(sdts.dates(end)) - datenum(sdts.dates(1))) / 5 + datenum(sdts.dates(1)), 'c');
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(pBOLL, 'k');
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h(1),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(2),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(3),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MLCON10);
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MLCON20);
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MLCON60);
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MHCON10);
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MHCON20);
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MHCON60);
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(BOLLWINDOW);
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
plot(pMOV); % MOV may change the limit of y a lot
plot(pricem(:, 1), pricem(:, 2), 'm');
ylim(yl); % set the limit of y
if strcmp(stock, 'SVXY')
    yl = ylim;
    yl = [50, 70];
    ylim(yl);
end;
if strcmp(stock, 'XIV')
    yl = ylim;
    patterns = {'-ob','-og','-or','-oc','-om','-oy','-ok'};
    for i=1:length(tsnet3)
        h = plot(tsnet3{i}*(yl(2)-yl(1))+yl(1), patterns{i});
    end;
    patterns = {'-xb','-xg','-xr','-xc','-xm','-xy','-xk'};
    for i=1:length(tsnet4)
        h = plot(tsnet4{i}*(yl(2)-yl(1))+yl(1), patterns{i});
    end;
    patterns = {'-+b','-+g','-+r','-+c','-+m','-+y','-+k'};
    for i=1:length(tsnet5)
        h = plot(tsnet5{i}*(yl(2)-yl(1))+yl(1), patterns{i});
    end;
    patterns = {'-sb','-sg','-sr','-sc','-sm','-sy','-sk'};
    for i=1:length(tsnet6)
        h = plot(tsnet6{i}*(yl(2)-yl(1))+yl(1), patterns{i});
    end;
end;
grid minor;
xl = xlim;
xd = (xl(2) - xl(1)) / 12;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
if strcmp(stock, 'XIV')
    lds = {'MOV200', 'MOV100', 'MOV50', 'LINE-20'};
    for i=1:length(tsnet3)
        lds{4+i} = ['RiseFall3.',num2str(i)];
    end;
    for i=1:length(tsnet4)
        lds{4+length(tsnet3)+i} = ['RiseFall4.',num2str(i)];
    end;
    for i=1:length(tsnet5)
        lds{4+length(tsnet3)+length(tsnet4)+i} = ['RiseFall5.',num2str(i)];
    end;
    for i=1:length(tsnet6)
        lds{4+length(tsnet3)+length(tsnet4)+length(tsnet5)+i} = ['RiseFall6.',num2str(i)];
    end;
    legend(lds);
else
    legend({'MOV200', 'MOV100', 'MOV50', 'LINE-20'});
end;
legend('Location','North');
%legend('hide');
set(gca, 'YAxisLocation', 'right');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

subplot(2, 1, 2);
hold on;
lh = plot([fdts.STBE, fdts.STBU, fdts.LTBE, fdts.LTBU, fdts.SENTI * 0.05, fdts.PDIV + 0.025], '-+');
set(lh, 'MarkerSize', 4);
legds = {'STBE', 'STBU', 'LTBE', 'LTBU', 'SENTI', 'PDIV'};
i = 7;
if ~isempty(buy)
    plot(buy * 0.01, 'go');
    legds(i) = {'BUY'};
    i = i + 1;
end;
if ~isempty(sell)
    plot(sell * 0.01, 'rx');
    legds(i) = {'SELL'};
    i = i + 1;
end;
legds(i) = {'VOLUME'};
Mvol = max(sdts.VOLUME);
Mvol = Mvol.VOLUME;
lh = plot(sdts.VOLUME / Mvol * 0.015 + 0.035, 'k-+');
set(lh, 'MarkerSize', 4);
legend(legds);
title(stock);
ylim([0, 0.05]);
grid minor;
xl = xlim;
xd = (xl(2) - xl(1)) / 12;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
legend('Location','SouthWest');
set(gca, 'YAxisLocation', 'right');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

set(fh, 'PaperSize',[22,17], 'PaperPositionMode','auto', 'PaperPosition',[-1,-1,24,18.5]);
print(['fig/', stock, '-p.eps'], '-dpsc2')
if backend
    close(fh);
end;

figure(2); close(2);
fh = figure(2);
if backend
    set(fh, 'Visible', 'off');
end;

subplot(3, 1, 1);
lh = plot(fdts.TRTDVOL, '-+');
set(lh, 'MarkerSize', 4);
hold on;
plot(trtdl(:, 1), trtdl(:, 2), 'r', trtdm(:, 1), trtdm(:, 2), 'm', trtds(:, 1), trtds(:, 2), 'g');
grid minor;
xl = xlim;
xd = (xl(2) - xl(1)) / 4;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
legend({'TRTDVOL', 'LINE-40', 'LINE-20', 'LINE-10'});
legend('Location', 'SouthWest');
set(gca, 'YAxisLocation', 'right');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

subplot(3, 1, 2);
lh = plot(fdts.WADVOL, '-+');
set(lh, 'MarkerSize', 4);
hold on;
plot(wadl(:, 1), wadl(:, 2), 'r', wads(:, 1), wads(:, 2), 'g');
grid minor;
xl = xlim;
xd = (xl(2) - xl(1)) / 12;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
legend({'WADVOL', 'LINE-40', 'LINE-10'});
legend('Location', 'SouthWest');
set(gca, 'YAxisLocation', 'right');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

subplot(3, 1, 3);
lh = plot(fdts.SENTI, '-+');
set(lh, 'MarkerSize', 4);
ylim([0.2, 0.8]);
hold on;
plot(stis(:, 1), stis(:, 2), 'g', stil(:, 1), stil(:, 2), 'r');
grid minor;
xl = xlim;
xd = (xl(2) - xl(1)) / 12;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
legend({'SENTI', 'LINE-10', 'LINE-40'});
legend('Location','SouthWest');
title(stock);
set(gca, 'YAxisLocation', 'right');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

set(fh, 'PaperSize',[22,17], 'PaperPositionMode','auto', 'PaperPosition',[-1,-1,24,18.5]);
print(['fig/', stock, '-t.eps'], '-dpsc2')
if backend
    close(fh);
end;

if strcmp(stock, 'XIV')
    drawxivnnet;
end;
