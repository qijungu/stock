#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    char stock[10];
    char cmd[1000];
    if (argc == 1) {
        printf("./drawstock all [startday [endday]]\n");
        printf("./drawstock stock [startday [endday]]\n");
        printf("\n");
        return 1;
    }
    int startday = 252;
    int endday = 0;
    if (argc >= 3) startday = atoi(argv[2]);
    if (argc >= 4) endday = atoi(argv[3]);
    if (strncmp(argv[1], "all", 3) == 0) {
        printf("Drawing all stocks\n\n");
        FILE* fp = fopen("stock.list", "r");
        while(fscanf(fp, "%s", stock)) {
            if (feof(fp)) break; // reach the end of stock list
            if (stock[0] == '#') continue; // comment out
            printf("Drawing %s\n", stock);
            //sprintf(cmd, "./rundrawchart.sh $MATLAB %s 1 %d %d", stock, startday, endday);
            sprintf(cmd, "matlab 1>/dev/null -nosplash -nodesktop -r \"drawchart(\'%s\', \'1\', \'%d\', \'%d\'); exit\"", stock, startday, endday);
            if (system(cmd) < 0) break;
        }
        fclose(fp);
    } else {
        strncpy(stock, argv[1], 10);
        //sprintf(cmd, "./rundrawchart.sh $MATLAB %s 0 %d %d", stock, startday, endday);
        sprintf(cmd, "matlab 1>/dev/null -nosplash -nodesktop -r \"drawchart(\'%s\', \'1\', \'%d\', \'%d\'); exit\"", stock, startday, endday);
        system(cmd);
    }
    printf("\n");
    return 0;
}
