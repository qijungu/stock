function analyzeshort(stock)

stock = upper(stock);
stockdata = ['data/matlab/', stock, '.dat'];
ts = ascii2fts(stockdata, 1, 2, []);
len = length(ts);
window = 10;
lossmargin = 0.4;
wr = willpctr(ts, window);

gain = zeros(len, 2);
for i = window + 1 : len
    yed = i - 2; % yesterday
    tod = i - 1; % today
    tmd = i;     % tomorrow
    % rule 7, open/clos with bailout || rule 8, %r with bailout
    if (getfield(ts(tod), 'CLOSE') < getfield(ts(yed), 'CLOSE') && getfield(ts(tod), 'CLOSE') > getfield(ts(tod), 'OPEN')) || (getfield(ts(tod), 'CLOSE') < getfield(ts(yed), 'LOW') && 100 + getfield(wr(tod), 'WillPctR') < 20) && weekday(ts.dates(tod)) ~= 5
        if getfield(ts(tmd), 'HIGH') >= getfield(ts(tod), 'HIGH') % tmorrow's high is higher than today's high
            if (getfield(ts(tmd), 'OPEN') <= getfield(ts(tod), 'HIGH'))
                buy = getfield(ts(tod), 'HIGH'); % buy tomorrow at today's high if tommorrow opens lower than today's high
            else
                buy = getfield(ts(tmd), 'OPEN'); % buy tomorrow at tomorrow's open if tomorrow opens higher than today's high
            end;
            vlth = getfield(ts(tmd - window : tod), 'HIGH');
            vltl = getfield(ts(tmd - window : tod), 'LOW');
            vltrng = vlth - vltl;
            vlt = median(vltrng);
            stop = buy - vlt * lossmargin;
            sold = false;
            j = tmd + 1;
            while ~sold & j <= len
                if getfield(ts(j), 'OPEN') > buy
                    sold = true;
                    gain(i, 1) = getfield(ts(j), 'OPEN') - buy; % sell at the first gain open
                    gain(i, 2) = j - i;
                elseif getfield(ts(j), 'LOW') < stop
                    sold = true;
                    gain(i, 1) = min([stop, getfield(ts(j), 'OPEN')]) - buy; % sell at open or stop whichever is lower
                    gain(i, 2) = j - i;
                else
                    j = j + 1;
                end;
            end;
        end;
    end;
end;

gainidx = find(gain(:, 1) > 0);
gainday = ts.dates(gainidx);
gaints = fints(gainday, gain(gainidx), 'GAIN')
gaincnt = length(gainidx)
gainsum = sum(gain(gainidx, 1))
[min(gain(gainidx, 1)), median(gain(gainidx, 1)), max(gain(gainidx, 1))]
[min(gain(gainidx, 2)), median(gain(gainidx, 2)), max(gain(gainidx, 2))]
lossidx = find(gain(:, 1) < 0);
lossday = ts.dates(lossidx);
lossts = fints(lossday, gain(lossidx), 'LOSS')
losscnt = length(lossidx)
losssum = sum(gain(lossidx, 1))
[min(gain(lossidx, 1)), median(gain(lossidx, 1)), max(gain(lossidx, 1))]
[min(gain(lossidx, 2)), median(gain(lossidx, 2)), max(gain(lossidx, 2))]

subplot(2, 1, 1);
plot(ts.CLOSE);
subplot(2, 1, 2);
plot(fints(ts.dates, gain(:, 1)), 'o');
title(stock);
