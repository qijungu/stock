function [buy, measure] = buysignal(div)
% div is a ts of divergence signaL
% return true if buy

len = length(div);
date = getfield(div, 'dates');
stbe = getfield(div, 'STBE');
stbek = regress(stbe, [date, ones(len, 1)]);
stbek = stbek(1);
stbu = getfield(div, 'STBU');
stbuk = regress(stbu, [date, ones(len, 1)]);
stbuk = stbuk(1);
% mtbe = getfield(div, 'MTBE');
% mtbek = regress(mtbe, [date, ones(len, 1)]);
% mtbek = mtbek(1);
% mtbu = getfield(div, 'MTBU');
% mtbuk = regress(mtbu, [date, ones(len, 1)]);
% mtbuk = mtbuk(1);
ltbe = getfield(div, 'LTBE');
ltbek = regress(ltbe, [date, ones(len, 1)]);
ltbek = ltbek(1);
ltbu = getfield(div, 'LTBU');
ltbuk = regress(ltbu, [date, ones(len, 1)]);
ltbuk = ltbuk(1);
sti = getfield(div, 'SENTI');
stik = regress(sti, [date, ones(len, 1)]);
stik = stik(1);
pdiv = getfield(div, 'PDIV');
pdivk = regress(pdiv, [date, ones(len, 1)]);
pdivk = pdivk(1);

% pdiv bottoms below -0.012
if peak(-pdiv) && min(pdiv) < -0.012
    buy = 5;

% pdiv bottoms below -0.01, and stbu is above 0.015.
elseif peak(-pdiv) && min(pdiv) < -0.01 && max(stbu) > 0.015
    buy = 4.5;

% the change from long term bear to bull
elseif ltbuk > 0 && ltbek < 0
    buy = 4.0;
    
else
    buy = 0;
end;

measure = [max(stbe), max(stbu), max(ltbe), max(ltbu), min(sti)];
