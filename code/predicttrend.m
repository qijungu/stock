function predicttrend(varargin)
% predicttrend(stock)
% default: stock = 'SPY'

stock = upper('qqq');
days = 126; % 126, half year

argsz = length(varargin);
if argsz >= 1
    stock = upper(varargin{1});
end;

figdata=['fig/data/', stock, '.dat'];
fdts = ascii2fts(figdata, 1, 2, []);
fdts = fdts(end - days : end);

tt = normalizets(fdts.TRTDVOL);
st = fdts.SENTI;

s{1} = sprintf('%-8s\tShort\t\tLong\t\t', stock);
s{2} = sprintf('        TRTD\tSTI\tTRTD\tSTI\t');

% original
sduration = 10;
lduration = 40;

trtdsmat = fts2mat(tt(end - sduration + 1: end), 1);
trtdscoef = regress(trtdsmat(:, 2), [trtdsmat(:, 1), ones(sduration, 1)]);
trtdlmat = fts2mat(tt(end - lduration + 1: end), 1);
trtdlcoef = regress(trtdlmat(:, 2), [trtdlmat(:, 1), ones(lduration, 1)]);

stismat = fts2mat(st(end - sduration + 1: end), 1);
stiscoef = regress(stismat(:, 2), [stismat(:, 1), ones(sduration, 1)]);
stilmat = fts2mat(st(end - lduration + 1: end), 1);
stilcoef = regress(stilmat(:, 2), [stilmat(:, 1), ones(lduration, 1)]);

ko = atan([trtdscoef(1), stiscoef(1), trtdlcoef(1), stilcoef(1)]);
s{3} = sprintf('origin: %s', sprintf('%+.4f\t', ko));

% predict

sduration = sduration - 1;
lduration = lduration - 1;

trtdsmat = fts2mat(tt(end - sduration + 1: end), 1);
trtdscoef = regress(trtdsmat(:, 2), [trtdsmat(:, 1), ones(sduration, 1)]);
trtdlmat = fts2mat(tt(end - lduration + 1: end), 1);
trtdlcoef = regress(trtdlmat(:, 2), [trtdlmat(:, 1), ones(lduration, 1)]);

stismat = fts2mat(st(end - sduration + 1: end), 1);
stiscoef = regress(stismat(:, 2), [stismat(:, 1), ones(sduration, 1)]);
stilmat = fts2mat(st(end - lduration + 1: end), 1);
stilcoef = regress(stilmat(:, 2), [stilmat(:, 1), ones(lduration, 1)]);

kp = atan([trtdscoef(1), stiscoef(1), trtdlcoef(1), stilcoef(1)]);
s{4} = sprintf('predic: %s', sprintf('%+.4f\t', kp));

s{5} = sprintf('change: %s', sprintf('%+.4f\t', kp - ko));

if ko(2) < ko(1) % stbu
    so = ko(1) - ko(2);
    sp = kp(1) - kp(2);
    if sp > so
        s{6} = sprintf('STBU may rise, if no abrupt change in volume or price.');
    else
        s{6} = sprintf('STBU may fall, if no abrupt change in volume or price.');
    end;
else % stbe
    so = ko(2) - ko(1);
    sp = kp(2) - kp(1);
    if sp > so
        s{6} = sprintf('STBE may rise, if no abrupt change in volume or price.');
    else
        s{6} = sprintf('STBE may fall, if no abrupt change in volume or price.');
    end;
end

if ko(4) < ko(3) % ltbu
    so = ko(3) - ko(4);
    sp = kp(3) - kp(4);
    if sp > so
        s{7} = sprintf('LTBU may rise, if no abrupt change in volume or price.');
    else
        s{7} = sprintf('LTBU may fall, if no abrupt change in volume or price.');
    end;
else % ltbe
    so = ko(4) - ko(3);
    sp = kp(4) - kp(3);
    if sp > so
        s{7} = sprintf('LTBE may rise, if no abrupt change in volume or price.');
    else
        s{7} = sprintf('LTBE may fall, if no abrupt change in volume or price.');
    end;
end

s(:)
