function [LCON, MLCON] = lowconcave(lowts)
% LCON is the time seriels of lows that make a concave of segments
% MLCON is the longest low segment

len = length(lowts);
low = fts2mat(lowts, 1);
x = low(:, 1); % dates
y = low(:, 2); % low
i = 1;
r = 1;
while i < len
    x0 = x(i);
    y0 = y(i);
    lcon(r, :) = [x0, y0];
    j = i + 1;
    while j <= len
        x1 = x(j);
        y1 = y(j);
        k = (y1 - y0) / (x1 - x0);
        concave = true;
        for q = [j + 1 : len]
            if y(q) < k * (x(q) - x0) + y0 % the point of q is below the line
                j = q;
                concave = false;
                break;
            end;
        end;
        if concave
            i = j;
            r = r + 1;
            break;
        end;
    end;
end;
lcon(r, :) = [x(len), y(len)];
LCON = fints(lcon(:, 1), lcon(:, 2), 'LCON');

len = length(LCON);
Mdst = 0;
for i = [1 : len - 1]
    dst = lcon(i + 1, 1) - lcon(i, 1);
    if  dst >= Mdst
        Mdst = dst;
        Mi = i;
    end;
end;
MLCON = fints(lcon(Mi : Mi + 1, 1), lcon(Mi : Mi + 1, 2), 'MLCON');
