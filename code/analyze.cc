#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    if (argc == 0) {
        printf("analyze [index.list] [stock.list] [mf.list]\n");
        exit(1);
    }
    char cmd[100];
    int i;
    FILE* fp;
    for (i = 1; i < argc; i++) {
        if (strcmp(argv[i], "stock.list") == 0) {
            char stock[10];
            printf("Analyzing all stocks\n\n");
            fp = fopen("stock.list", "r");
            while(fscanf(fp, "%s", stock)) {
                if (feof(fp)) break; // reach the end of stock list
                if (stock[0] == '#') continue; // comment out
                printf("Analyzing %s\n", stock);
                sprintf(cmd, "./runanalyzestock.sh $MATLAB %s", stock);
                if (system(cmd) < 0) break;
            }
            fclose(fp);
        } else if (strcmp(argv[i], "mf.list") == 0) {
            char mf[10];
            printf("Analyzing all mutual funds\n\n");
            fp = fopen("mf.list", "r");
            while(fscanf(fp, "%s", mf)) {
                if (feof(fp)) break; // reach the end of mf list
                if (mf[0] == '#') continue; // comment out
                printf("Analyzing %s\n", mf);
                sprintf(cmd, "./runanalyzemf.sh $MATLAB %s", mf);
                if (system(cmd) < 0) break;
            }
            fclose(fp);
        } else if (strcmp(argv[i], "index.list") == 0) {
            char index[10];
            printf("Analyzing all indices\n\n");
            fp = fopen("index.list", "r");
            while(fscanf(fp, "%s", index)) {
                if (feof(fp)) break; // reach the end of index list
                if (index[0] == '#') continue; // comment out
                printf("Analyzing %s\n", index);
                sprintf(cmd, "./runanalyzeindex.sh $MATLAB %s", index);
                if (system(cmd) < 0) break;
            }
            fclose(fp);
        } else {
            printf("Unknow list\n");
            exit(1);
        }
        printf("\n");
    }
    return 0;
}
