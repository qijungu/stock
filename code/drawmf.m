function drawmf(varargin)
% drawmf(mf, backend, startday, endday)
% backend: 1 no show figure, 0 show figure
% startday: the start day of the chart. -126 or 126 means 126 stock days before today
% endday: the end day of the chart. -1 or 1 means 1 stock day before today
% default: stock = 'SPY', backend = 0, startday = 126 or -126, endday = 0;

mf = upper('fbiox');
backend = 0;
startday = 504; % 126, half year, 252, 1 year
endday = 0;

argsz = length(varargin);
if argsz >= 1
    mf = upper(varargin{1});
end;
if argsz >= 2
    backend = str2num(varargin{2});
end;
if argsz >= 3
    startday = abs(str2num(varargin{3}));
end;
if argsz >= 4
    endday = abs(str2num(varargin{4}));
end;

mfdata = ['data/matlab/', mf, '.dat'];
mdts = ascii2fts(mfdata, 1, 2, []);
figdata = ['fig/data/', mf, '.dat'];
fdts = ascii2fts(figdata, 1, 2, []);

startday = min([startday, length(fdts) - 126]);
if endday >= startday - 5
    fprintf('The startday %d must be at least 5 days before the endday %d\n', startday, endday);
    return;
end;

mdts = mdts(end - startday : end - endday);
fdts = fdts(end - startday : end - endday);

date = getfield(mdts, 'dates');
len = length(mdts);

pCLOSE = mdts.CLOSE;
pMOV = [mdts.CLOSE, fdts.MOV200, fdts.MOV100, fdts.MOV50];
pBOLL = [fdts.MIDDLE, fdts.UPPER, fdts.LOWER];
pRET =[fdts.RET60, fdts.RET30];

lwindow = 60;
swindow = 30;
lret = [[mdts.dates(end - lwindow + 1), mdts.dates(end)]', [getfield(mdts(end - lwindow + 1), 'CLOSE'), getfield(mdts(end), 'CLOSE')]'];
sret = [[mdts.dates(end - swindow + 1), mdts.dates(end)]', [getfield(mdts(end - swindow + 1), 'CLOSE'), getfield(mdts(end), 'CLOSE')]'];

figure(1); close(1);
fh =figure(1);
if backend
    set(fh, 'Visible', 'off');
end;

subplot(2, 1, 1);
hold on;
h = plot(pCLOSE, '+');
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
plot(pMOV);
h = plot(pBOLL, 'k');
set(get(get(h(1),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(2),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(3),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(lret(:, 1), lret(:, 2), '-m');
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(sret(:, 1), sret(:, 2), '-m');
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
grid minor;
xl = xlim;
xd = (xl(2) - xl(1)) / 12;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
legend({'PRICE', 'MOV200', 'MOV100', 'MOV50'});
legend('Location','North');
set(gca, 'YAxisLocation', 'right');

subplot(2, 1, 2);
hold on;
h = plot(pRET, '-+');
set(h, 'MarkerSize', 4);
ylim([-0.15, 0.15]);
grid minor;
xl = xlim;
h = plot(xlim, [0, 0], '-r');
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
xd = (xl(2) - xl(1)) / 12;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
legend('Location','SouthWest');
set(gca, 'YAxisLocation', 'right');

set(fh, 'Position', [1, 1, 2800, 1800]);
set(gcf,'PaperPositionMode','auto');
saveas(fh, ['fig/mf/', mf, '.eps'], 'psc2');
if backend
    close(fh);
end;
