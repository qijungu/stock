function TT = truetrend(ts)
% netchange = close_today - open_today
% truerange = high - low
% changeratio = netchange / truerange * (volume?)

tslen = length(ts);
DATE = getfield(ts, 'dates');
high = getfield(ts, 'HIGH');
low = getfield(ts, 'LOW');
close = getfield(ts, 'CLOSE');
open = getfield(ts, 'OPEN');
vol = getfield(ts, 'VOLUME');

netchange = close -open;
truerange = high - low;
changert = netchange ./ truerange;
changert(find(isnan(changert))) = 1;
changertvol = changert .* vol;

changert = cumsum(changert);
changertvol = cumsum(changertvol);

TT = fints(DATE, [changert, changertvol], {'TRTD', 'TRTDVOL'});
