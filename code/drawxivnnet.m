function drawchart(varargin)
% drawchar(stock, backend, startday, endday)
% backend: 1 no show figure, 0 show figure
% startday: the start day of the chart. -126 or 126 means 126 stock days before today
% endday: the end day of the chart. -1 or 1 means 1 stock day before today
% default: stock = 'XIV', backend = 0, startday = 126 or -126, endday = 0;

netfile3 = {'ml/mlpr_xiv_sol3_0.037125.mat','ml/mlpr_xiv_sol3_0.037924.mat','ml/mlpr_xiv_sol3_0.043296.mat','ml/mlpr_xiv_sol3_0.043677.mat','ml/mlpr_xiv_sol3_0.044214.mat','ml/mlpr_xiv_sol3_0.044623.mat'}; %201712
netfile4 = {'ml/mlpr_xiv_sol4_0.024927.mat','ml/mlpr_xiv_sol4_0.025419.mat','ml/mlpr_xiv_sol4_0.028080.mat','ml/mlpr_xiv_sol4_0.028773.mat','ml/mlpr_xiv_sol4_0.029192.mat','ml/mlpr_xiv_sol4_0.029266.mat'}; %201712
netfile5 = {'ml/mlpr_xiv_sol5_0.029202.mat','ml/mlpr_xiv_sol5_0.030095.mat','ml/mlpr_xiv_sol5_0.031386.mat','ml/mlpr_xiv_sol5_0.032116.mat','ml/mlpr_xiv_sol5_0.034790.mat','ml/mlpr_xiv_sol5_0.037461.mat'}; %201712
netfile6 = {'ml/mlpr_xiv_sol6_0.015010.mat','ml/mlpr_xiv_sol6_0.022073.mat','ml/mlpr_xiv_sol6_0.023762.mat','ml/mlpr_xiv_sol6_0.024298.mat','ml/mlpr_xiv_sol6_0.024734.mat','ml/mlpr_xiv_sol6_0.024781.mat'}; %201712
numfeature3 = 15; % number of features
numfeature4 = 17; % number of features
numfeature5 = 30; % number of features
numfeature6 = 32; % number of features

stock = upper('xiv');
backend = 1;
startday = 120; % 126, half year, 252, 1 year
endday = 0;

argsz = length(varargin);
if argsz >= 1
    stock = upper(varargin{1});
end;
if argsz >= 2
    backend = str2num(varargin{2});
end;
if argsz >= 3
    startday = abs(str2num(varargin{3}));
end;
if argsz >= 4
    endday = abs(str2num(varargin{4}));
end;

stockdata = ['data/matlab/', stock, '.dat'];
sdts = ascii2fts(stockdata, 1, 2, []);
figdata = ['fig/data/', stock, '.dat'];
fdts = ascii2fts(figdata, 1, 2, []);

% startday = min([startday, length(fdts) - 126]);
startday = min([startday, length(fdts)]);
if endday >= startday - 5
    fprintf('The startday %d must be at least 5 days before the endday %d\n', startday, endday);
    return;
end;

sdts = sdts(end - startday : end - endday);
fdts = fdts(end - startday : end - endday);

date = getfield(sdts, 'dates');
len = length(sdts);

pBOLL = [fdts.MIDDLE, fdts.UPPER, fdts.LOWER];
pMOV = [fdts.MOV200, fdts.MOV100, fdts.MOV50];

buy = fts2mat(fdts.BUY);
sell = fts2mat(fdts.SELL);
idx = find(buy);
if isempty(idx)
    buy = [];
else
    buy = fints(fdts.dates(idx), buy(idx), 'BUY');
end;
idx = find(sell);
if isempty(idx) 
    sell = [];
else
    sell = fints(fdts.dates(idx), sell(idx), 'SELL');
end;

swindow = 4; % short window to get vix change rate
lwindow = 8; % long window to get vix change rate
delay = 60; % delay

tsxiv = ascii2fts('data/matlab/XIV.dat', 1, 2, []);
tsxiv = tsxiv(end - startday - delay*2 : end - endday);
tsspy = ascii2fts('data/matlab/SPY.dat', 1, 2, []);
tsspy = tsspy(end - startday - delay*2 : end - endday);
tsvix = ascii2fts('data/matlab/VIX.dat', 1, 2, []);
tsvix = tsvix(end - startday - delay*2 : end - endday);

% p: price, v: volume, c: change, b: bolling, rs: ret short, rl: ret long
xivp = fts2mat(tsxiv, 0, 'CLOSE');
xivh = fts2mat(tsxiv, 0, 'HIGH');
xivl = fts2mat(tsxiv, 0, 'LOW');
xivo = fts2mat(tsxiv, 0, 'OPEN');
xivv = fts2mat(tsxiv, 0, 'VOLUME');
xivc = xivp(2:end) ./ xivp(1:end-1) - 1;
[xivmid, xivup, xivlow] = bollinger(tsxiv);
xivb = fts2mat(xivup.CLOSE) - fts2mat(xivlow.CLOSE);
xivup=fts2mat(xivup, 'CLOSE');
xivlow=fts2mat(xivlow, 'CLOSE');
xivrs = xivp(swindow+1:end)./xivp(swindow/2+1:end-swindow/2) + xivp(swindow/2+1:end-swindow/2)./xivp(1:end-swindow);
xivrl = xivp(lwindow+1:end)./xivp(lwindow/2+1:end-lwindow/2) + xivp(lwindow/2+1:end-lwindow/2)./xivp(1:end-lwindow);
spyp = fts2mat(tsspy, 0, 'CLOSE');
spyh = fts2mat(tsspy, 0, 'HIGH');
spyl = fts2mat(tsspy, 0, 'LOW');
spyo = fts2mat(tsspy, 0, 'OPEN');
spyv = fts2mat(tsspy, 0, 'VOLUME');
spyc = spyp(2:end) ./ spyp(1:end-1) - 1;
[spymid, spyup, spylow] = bollinger(tsspy);
spyb = fts2mat(spyup.CLOSE) - fts2mat(spylow.CLOSE);
spyup=fts2mat(spyup, 'CLOSE');
spylow=fts2mat(spylow, 'CLOSE');
spyrs = spyp(swindow+1:end)./spyp(swindow/2+1:end-swindow/2) + spyp(swindow/2+1:end-swindow/2)./spyp(1:end-swindow);
spyrl = spyp(lwindow+1:end)./spyp(lwindow/2+1:end-lwindow/2) + spyp(lwindow/2+1:end-lwindow/2)./spyp(1:end-lwindow);
[vixmid, vixup, vixlow] = bollinger(tsvix);
vixb = fts2mat(vixup.CLOSE) - fts2mat(vixlow.CLOSE);
vixp = fts2mat(tsvix, 0, 'CLOSE');
vixh = fts2mat(tsvix, 0, 'HIGH');
vixl = fts2mat(tsvix, 0, 'LOW');
vixo = fts2mat(tsvix, 0, 'OPEN');
vixc = vixp(2:end) ./ vixp(1:end-1) - 1;
vixpn = (vixp - fts2mat(vixmid.CLOSE)) ./ vixb; % normalized vixp
vixcn = vixpn(2:end) - vixpn(1:end-1); % normalized vixp change rate
vixup=fts2mat(vixup, 'CLOSE');
vixlow=fts2mat(vixlow, 'CLOSE');
vixrs = vixp(swindow+1:end)./vixp(swindow/2+1:end-swindow/2) + vixp(swindow/2+1:end-swindow/2)./vixp(1:end-swindow);
vixrl = vixp(lwindow+1:end)./vixp(lwindow/2+1:end-lwindow/2) + vixp(lwindow/2+1:end-lwindow/2)./vixp(1:end-lwindow);

inputs3 = zeros(numfeature3*delay, len);
for i=0:delay-1
    inputs3(i*numfeature3+1:(i+1)*numfeature3,:)=[
    xivp(end-i-len+1:end-i)';
    xivv(end-i-len+1:end-i)';
    xivc(end-i-len+1:end-i)';
    xivb(end-i-len+1:end-i)';
    spyp(end-i-len+1:end-i)';
    spyv(end-i-len+1:end-i)';
    spyc(end-i-len+1:end-i)';
    spyb(end-i-len+1:end-i)';
    vixp(end-i-len+1:end-i)';
    vixc(end-i-len+1:end-i)';
    vixb(end-i-len+1:end-i)';
    vixup(end-i-len+1:end-i)';
    vixlow(end-i-len+1:end-i)';
    vixrs(end-i-len+1:end-i)';
    vixrl(end-i-len+1:end-i)';
    ];
end;

tsnetlabel3 = 'RiseFall3.';
tsnet3 = {};
for i=1:length(netfile3)
    nf3 = netfile3{i};
    net3 = load(nf3);
    net3 = net3.net;
    outputs3 = net3(inputs3);
    a=tsxiv(end-len+1:end);
    tsnet3{i} = fints(a.dates, outputs3',{'RiseFall'});
end;

inputs4 = zeros(numfeature4*delay, len);
for i=0:delay-1
    inputs4(i*numfeature4+1:(i+1)*numfeature4,:)=[
    xivp(end-i-len+1:end-i)';
    xivv(end-i-len+1:end-i)';
    xivc(end-i-len+1:end-i)';
    xivb(end-i-len+1:end-i)';
    spyp(end-i-len+1:end-i)';
    spyv(end-i-len+1:end-i)';
    spyc(end-i-len+1:end-i)';
    spyb(end-i-len+1:end-i)';
    vixp(end-i-len+1:end-i)';
    vixc(end-i-len+1:end-i)';
    vixpn(end-i-len+1:end-i)';
    vixcn(end-i-len+1:end-i)';
    vixb(end-i-len+1:end-i)';
    vixup(end-i-len+1:end-i)';
    vixlow(end-i-len+1:end-i)';
    vixrs(end-i-len+1:end-i)';
    vixrl(end-i-len+1:end-i)';
    ];
end;

tsnetlabel4 = 'RiseFall4.';
tsnet4 = {};
for i=1:length(netfile4)
    nf4 = netfile4{i};
    net4 = load(nf4);
    net4 = net4.net;
    outputs4 = net4(inputs4);
    a=tsxiv(end-len+1:end);
    tsnet4{i} = fints(a.dates, outputs4',{'RiseFall'});
end;

inputs5 = zeros(numfeature5*delay, len);
for i=0:delay-1
    inputs5(i*numfeature5+1:(i+1)*numfeature5,:)=[
    xivp(end-i-len+1:end-i)';
    xivh(end-i-len+1:end-i)';
    xivl(end-i-len+1:end-i)';
    xivo(end-i-len+1:end-i)';
    xivv(end-i-len+1:end-i)';
    xivb(end-i-len+1:end-i)';
    xivup(end-i-len+1:end-i)';
    xivlow(end-i-len+1:end-i)';
    xivc(end-i-len+1:end-i)';
    spyp(end-i-len+1:end-i)';
    spyh(end-i-len+1:end-i)';
    spyl(end-i-len+1:end-i)';
    spyo(end-i-len+1:end-i)';
    spyv(end-i-len+1:end-i)';
    spyc(end-i-len+1:end-i)';
    spyb(end-i-len+1:end-i)';
    spyup(end-i-len+1:end-i)';
    spylow(end-i-len+1:end-i)';
    spyrs(end-i-len+1:end-i)';
    spyrl(end-i-len+1:end-i)';
    vixp(end-i-len+1:end-i)';
    vixh(end-i-len+1:end-i)';
    vixl(end-i-len+1:end-i)';
    vixo(end-i-len+1:end-i)';
    vixc(end-i-len+1:end-i)';
    vixb(end-i-len+1:end-i)';
    vixup(end-i-len+1:end-i)';
    vixlow(end-i-len+1:end-i)';
    vixrs(end-i-len+1:end-i)';
    vixrl(end-i-len+1:end-i)';
    ];
end;

tsnetlabel5 = 'RiseFall5.';
tsnet5 = {};
for i=1:length(netfile5)
    nf5 = netfile5{i};
    net5 = load(nf5);
    net5 = net5.net;
    outputs5 = net5(inputs5);
    a=tsxiv(end-len+1:end);
    tsnet5{i} = fints(a.dates, outputs5',{'RiseFall'});
end;

inputs6 = zeros(numfeature6*delay, len);
for i=0:delay-1
    inputs6(i*numfeature6+1:(i+1)*numfeature6,:)=[
    xivp(end-i-len+1:end-i)';
    xivh(end-i-len+1:end-i)';
    xivl(end-i-len+1:end-i)';
    xivo(end-i-len+1:end-i)';
    xivv(end-i-len+1:end-i)';
    xivb(end-i-len+1:end-i)';
    xivup(end-i-len+1:end-i)';
    xivlow(end-i-len+1:end-i)';
    xivc(end-i-len+1:end-i)';
    spyp(end-i-len+1:end-i)';
    spyh(end-i-len+1:end-i)';
    spyl(end-i-len+1:end-i)';
    spyo(end-i-len+1:end-i)';
    spyv(end-i-len+1:end-i)';
    spyc(end-i-len+1:end-i)';
    spyb(end-i-len+1:end-i)';
    spyup(end-i-len+1:end-i)';
    spylow(end-i-len+1:end-i)';
    spyrs(end-i-len+1:end-i)';
    spyrl(end-i-len+1:end-i)';
    vixp(end-i-len+1:end-i)';
    vixh(end-i-len+1:end-i)';
    vixl(end-i-len+1:end-i)';
    vixo(end-i-len+1:end-i)';
    vixc(end-i-len+1:end-i)';
    vixpn(end-i-len+1:end-i)';
    vixcn(end-i-len+1:end-i)';
    vixb(end-i-len+1:end-i)';
    vixup(end-i-len+1:end-i)';
    vixlow(end-i-len+1:end-i)';
    vixrs(end-i-len+1:end-i)';
    vixrl(end-i-len+1:end-i)';
    ];
end;

tsnetlabel6 = 'RiseFall6.';
tsnet6 = {};
for i=1:length(netfile6)
    nf6 = netfile6{i};
    net6 = load(nf6);
    net6 = net6.net;
    outputs6 = net6(inputs6);
    a=tsxiv(end-len+1:end);
    tsnet6{i} = fints(a.dates, outputs6',{'RiseFall'});
end;

figure(1); close(1);
fh =figure(1);
if backend
    set(fh, 'Visible', 'off');
end;

subplot(2, 2, 1);
hold on;
plotxivnnet(sdts,pBOLL,pMOV,tsnet3,tsnetlabel3)

subplot(2, 2, 2);
hold on;
plotxivnnet(sdts,pBOLL,pMOV,tsnet4,tsnetlabel4)

subplot(2, 2, 3);
hold on;
plotxivnnet(sdts,pBOLL,pMOV,tsnet5,tsnetlabel5)

subplot(2, 2, 4);
hold on;
plotxivnnet(sdts,pBOLL,pMOV,tsnet6,tsnetlabel6)

set(fh, 'PaperSize',[22,17], 'PaperPositionMode','auto', 'PaperPosition',[-1,-1,24,18.5]);
print(['fig/', stock, '-nnet.eps'], '-dpsc2')
if backend
    close(fh);
end;

'end'
return;

function plotxivnnet(sdts,pBOLL,pMOV,tsnet,tsnetlabel)

h = candle(sdts);
yl = ylim;
set(get(get(h(1),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(2),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(3),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(pBOLL, 'k');
yl = [min([yl ylim]), max([yl ylim])];
set(get(get(h(1),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(2),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(3),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
plot(pMOV); % MOV may change the limit of y a lot
ylim(yl); % set the limit of y
yl = ylim;
patterns = {'-ob','-og','-or','-oc','-om','-oy','-ok'};
for i=1:length(tsnet)
    h = plot(tsnet{i}*(yl(2)-yl(1)) + yl(1), patterns{i});
end;
grid minor;
xl = xlim;
xd = (xl(2) - xl(1)) / 6;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
lds = {'MOV200', 'MOV100', 'MOV50'};
for i=1:length(tsnet)
    lds{3+i} = [tsnetlabel,num2str(i)];
end;
legend(lds);
legend('Location','North');
set(gca, 'YAxisLocation', 'right');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

return;
