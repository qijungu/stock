#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <set>
#include <string>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#define YEARSTART 2000;
#define MONTHSTART 1;
#define DAYSTART 1;

struct cmpstr {
  bool operator()(const std::string& s1, const std::string& s2) const {
    return s1 < s2;
  }
};

int main(int argc, char** argv) {

    // arguments check
    if (argc < 2) {
        perror("Example: ./purge stock.list etf.list mf.list");
        return 1;
    }

    // read stock list
    int i;
    std::set<std::string, cmpstr> list;
    char stock[15];
    for (i =  1; i < argc; i++) {
        FILE* fplist;
        fplist = fopen(argv[i], "r");
        if (!fplist) {
            perror("Cannot open stock list");
            return 1;
        }
        while(fscanf(fplist, "%s", stock)) {
            if (feof(fplist)) break; // reach the end of stock list
            list.insert(std::string(stock));
        }
        fclose(fplist);
    }

    // read list in data folder
    std::set<std::string, cmpstr> dlist;
    DIR* dpdata = opendir("data");
    struct dirent* dir;
    struct stat fst;
    char stockdata[1000];
    while ((dir = readdir(dpdata))) {
        sprintf(stockdata, "data/%s", dir->d_name);
        lstat(stockdata, &fst);
        if (!S_ISREG(fst.st_mode)) continue;
        dlist.insert(std::string(dir->d_name));
    }
    closedir(dpdata);
    // clean
    char cmd[1000];
    std::set<std::string, cmpstr>::iterator si;
    std::string sd;
    for (si = dlist.begin(); si != dlist.end(); ++si) {
        sd = (*si);
        sd.resize(sd.size() - 4);
        if (list.count(sd) > 0) continue;
        printf("remove %s\n", (*si).c_str());
        snprintf(cmd, 1000, "rm -rf data/%s", (*si).c_str());
        assert(system(cmd) == 0);
        snprintf(cmd, 1000, "rm -rf data/matlab/%s", (*si).c_str());
        assert(system(cmd) == 0);
	snprintf(cmd, 1000, "rm -rf fig/data/%s", (*si).c_str());
        assert(system(cmd) == 0);
        snprintf(cmd, 1000, "rm -rf fig/%s-p.eps fig/%s-t.eps", sd.c_str(), sd.c_str());
        assert(system(cmd) == 0);
    }
    printf("\n");

    return 0;
}

