function STI = sentiment(ts, duration)
% average(close - truelow, duration) / average(truehigh - truelow, duration) * 100
% duration is the period of average

tslen = length(ts);
[TH, TL] = trueHL(ts);
close = getfield(ts, 'CLOSE');
date = getfield(ts, 'dates');
vol = getfield(ts, 'VOLUME');

sentiup = tsmovavg((close - TL), 's', duration, 1);
sentidn = tsmovavg((TH - TL), 's', duration, 1);

sentiupvol = tsmovavg((close - TL) .* vol, 's', duration, 1);
sentidnvol = tsmovavg((TH - TL) .* vol, 's', duration, 1);

STI = fints(date, sentiupvol ./ sentidnvol, 'SENTI');
