# Strike, Last, Change, Bid, Ask, Volume, Open Int
# struct optionrow {
#     double strike;
#     double bidc, askc;
#     long volc, openc;
#     double bidp, askp;
#     long volp, openp;
# };
import sys

def readoptions(fp) :
    fp.readline(1000)
    fp.readline(1000)
    buf = fp.readline(1000)
    opts = []
    while buf[0] != '=' :
        x = {}
        v = buf.split()
        x["bidc"] = float(v[0].split(",")[0])
        x["askc"] = float(v[1].split(",")[0])
        x["volc"] = int(v[2].split(",")[0])
        x["openc"] = int(v[3].split(",")[0])
        x["strike"] = float(v[4].split(",")[0])
        x["bidp"] = float(v[5].split(",")[0])
        x["askp"] = float(v[6].split(",")[0])
        x["volp"] = int(v[7].split(",")[0])
        x["openp"] = int(v[8].split(",")[0])
        opts.append(x)
        buf = fp.readline(1000)
    return opts

def main():

    if len(sys.argv) != 2 :
        print "python currentpain.py stock\n"
        print "compare current option with option at option/base.\n"
        return

    stock = sys.argv[1].upper()
    currentoption = "option/%s.max" % stock
    baseoption = "option/base/%s.max" % stock

    fcurropt = open(currentoption, "r")
    fbaseopt = open(baseoption, "r")
    if fcurropt == None or fbaseopt == None :
        return

    # read current options
    copts = readoptions(fcurropt)
    fcurropt.close()
    # read base options
    bopts = readoptions(fbaseopt)
    fbaseopt.close()

    ci = 0
    bi = 0
    painopts = []
    while ci < len(copts) or bi < len(bopts) :
        if (ci == len(copts) and bi < len(bopts)) or (ci < len(copts) and bi < len(bopts) and copts[ci]["strike"] > bopts[bi]["strike"]) :
            bi += 1
        elif (bi == len(bopts) and ci < len(copts)) or (ci < len(copts) and bi < len(bopts) and copts[ci]["strike"] < bopts[bi]["strike"]) :
            opt = copts[ci]
            painopts.append(opt)
            ci += 1
        else :
            assert(copts[ci]["strike"] == bopts[bi]["strike"])
            opt = copts[ci]
            opt["openc"] = copts[ci]["openc"] - bopts[bi]["openc"]
            if opt["openc"] < 0 :
                opt["openc"] = 0
            opt["openp"] = copts[ci]["openp"] - bopts[bi]["openp"]
            if opt["openp"] < 0 :
                opt["openp"] = 0
            painopts.append(opt)
            ci += 1
            bi += 1
    
    currentoption = "option/%s.curr" % stock
    fcurropt = open(currentoption, "w")
    fcurropt.write("                      Call                ,   %6s,                       Put                 \n" % stock)
    fcurropt.write("     Bid,      Ask,        Vol,   Open Int,         ,      Bid,      Ask,        Vol,   Open Int\n")
    for i in range(0, len(painopts)) :
        fcurropt.write("% 8.2lf, % 8.2lf, % 10ld, % 10ld, % 8.2lf, % 8.2lf, % 8.2lf, % 10ld, % 10ld\n" % (
                        painopts[i]["bidc"], painopts[i]["askc"], painopts[i]["volc"], painopts[i]["openc"],
                        painopts[i]["strike"],
                        painopts[i]["bidp"], painopts[i]["askp"], painopts[i]["volp"], painopts[i]["openp"]
                        )
                    )        
    
    maxstrike = -1.0
    maxpain = -1.0
    strike = painopts[0]["strike"]
    while strike <= painopts[-1]["strike"] :
        pain = 0.0
        for i in range(0, len(painopts)) :
            if painopts[i]["strike"] <= strike :
                pain += (strike - painopts[i]["strike"]) * painopts[i]["openc"]
            if painopts[i]["strike"] >= strike :
                pain += (painopts[i]["strike"] - strike) * painopts[i]["openp"]
        if maxpain < 0 or pain < maxpain :
            maxpain = pain
            maxstrike = strike
        strike += 0.25

    ccnt = 0.0 # simply count open interest
    pcnt = 0.0
    ceffect = 0.0 # count open interest with price
    peffect = 0.0
    cprice = 0.0 # count open interest multiply price
    pprice = 0.0
    for i in range(0, len(painopts)) :
        ccnt += painopts[i]["openc"]
        pcnt += painopts[i]["openp"]
        if painopts[i]["bidc"] > 0.03 and painopts[i]["askc"] > 0.03 :
            ceffect += painopts[i]["openc"]
            cprice += painopts[i]["openc"] * (painopts[i]["bidc"] + painopts[i]["askc"]) / 2
        if painopts[i]["bidp"] > 0.03 and painopts[i]["askp"] > 0.03 :
            peffect += painopts[i]["openp"]
            pprice += painopts[i]["openp"] * (painopts[i]["bidp"] + painopts[i]["askp"]) / 2

    if ccnt == 0 :
        ratio = -1
    else :                
        ratio = pcnt / ccnt
    if ceffect == 0 :
        ratioeffect = -1
    else :
        ratioeffect = peffect / ceffect
    if cprice == 0 :
        ratioprice = -1
    else :
        ratioprice = pprice / cprice

    fcurropt.write("====== current pain is at %0.2lf, priced p/c ratio %0.2lf ======\n" % (maxstrike, ratioprice))
    fcurropt.write("p/c ratio is %0.2lf, put %0.0lf, call %0.0lf, total %0.0lf\n" % (ratio, pcnt, ccnt, pcnt + ccnt))
    fcurropt.write("P/C ratio is %0.2lf, put %0.0lf, call %0.0lf, total %0.0lf (with effective price)\n" % (ratioeffect, peffect, ceffect, peffect + ceffect))

    fcurropt.close()

main()
