function normts = normalizets(ts)
fname = fieldnames(ts, 1);
mts = min(ts);
mts = getfield(mts, fname{1});
Mts = max(ts);
Mts = getfield(Mts, fname{1});
range = Mts -mts;
if range == 0
    normts = (ts - mts) + 0.5;
else
    normts = (ts - mts) / range;
end;
