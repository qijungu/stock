function testlongvlt(stock)

stock = upper(stock);
stockdata = ['data/matlab/', stock, '.dat'];
ts = ascii2fts(stockdata, 1, 2, []);
len = length(ts);

start = 1500;

window = 5;
for i = [start : len]
    if mod(i, 100) == 0
        i
    end;
    div(i, :) = fts2mat(detectstock(ts, - (len + 1 - i), 1));
end;
div = fints(ts.dates, div, {'STBE', 'STBU', 'MTBE', 'MTBU', 'LTBE', 'LTBU'});

for i = [start : len]
    buy(i) = buysignal(div(end - len + i - window : end - len - 1 + i));
%     sell(i) = sellsignal(div(end - duration + i - window : end - duration - 1 + i));
end;

buyidx = find(buy > 0);
buylen = length(buyidx)
delta = zeros(1, buylen);
for bidx = [1 : buylen]
    i = buyidx(bidx);
    wts = toweekly(ts(i - 40: i));
    hwts = getfield(wts, 'HIGH');
    lwts = getfield(wts, 'LOW');
    range = median(hwts - lwts);
    buyprice = getfield(ts.CLOSE(i), 'CLOSE');
    lownext = getfield(min(ts.CLOSE(i + 1 : i + 5)), 'CLOSE');
    delta(bidx) = (buyprice - lownext) / range;
end;

delta = sort(delta);
[min(delta), median(delta), max(delta)]

