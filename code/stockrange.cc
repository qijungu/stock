#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    char stock[10];
    char cmd[100];
    if (argc == 1) {
        printf("./stockrange stock [period], default is w\n");
	printf("./stockrange stock [d|w|m|q]\n");
        printf("\n");
        return 1;
    }
    strncpy(stock, argv[1], 10);
    char period = 'w';
    if (argc >= 3) period = argv[2][0];
    printf("min\tmed\tmax\n");
    sprintf(cmd, "./rundetectrange.sh /opt/matlab %s %c", stock, period);
    system(cmd);
    return 0;
}
