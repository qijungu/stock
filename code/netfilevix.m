function [netfile3,netfile4,netfile5,netfile6]=netfilevix()
[netfile3,netfile4,netfile5,netfile6]=netfilevix_201711();
[netfile3,netfile4,netfile5,netfile6]=netfilevix_current();
return;

function [netfile3,netfile4,netfile5,netfile6]=netfilevix_201711()
netfile3 = {'ml/201711/mlpr_vix_sol3_0.039552.mat','ml/201711/mlpr_vix_sol3_0.040200.mat','ml/201711/mlpr_vix_sol3_0.041841.mat','ml/201711/mlpr_vix_sol3_0.043504.mat','ml/201711/mlpr_vix_sol3_0.043879.mat','ml/201711/mlpr_vix_sol3_0.044496.mat'};
netfile4 = {'ml/201711/mlpr_vix_sol4_0.026825.mat','ml/201711/mlpr_vix_sol4_0.030399.mat','ml/201711/mlpr_vix_sol4_0.030652.mat','ml/201711/mlpr_vix_sol4_0.031159.mat','ml/201711/mlpr_vix_sol4_0.031708.mat','ml/201711/mlpr_vix_sol4_0.033860.mat'};
netfile5 = {'ml/201711/mlpr_vix_sol5_0.029439.mat','ml/201711/mlpr_vix_sol5_0.031332.mat','ml/201711/mlpr_vix_sol5_0.031635.mat','ml/201711/mlpr_vix_sol5_0.032891.mat','ml/201711/mlpr_vix_sol5_0.033956.mat','ml/201711/mlpr_vix_sol5_0.035005.mat'};
netfile6 = {'ml/201711/mlpr_vix_sol6_0.027707.mat','ml/201711/mlpr_vix_sol6_0.033569.mat','ml/201711/mlpr_vix_sol6_0.034702.mat','ml/201711/mlpr_vix_sol6_0.036269.mat','ml/201711/mlpr_vix_sol6_0.037235.mat','ml/201711/mlpr_vix_sol6_0.037436.mat'};
return;

function [netfile3,netfile4,netfile5,netfile6]=netfilevix_current()
netfile3 = {'ml/mlpr_vix_sol3_0.037403.mat','ml/mlpr_vix_sol3_0.037747.mat','ml/mlpr_vix_sol3_0.038102.mat','ml/mlpr_vix_sol3_0.038733.mat','ml/mlpr_vix_sol3_0.039315.mat','ml/mlpr_vix_sol3_0.040948.mat',};
netfile4 = {'ml/mlpr_vix_sol4_0.037431.mat','ml/mlpr_vix_sol4_0.039375.mat','ml/mlpr_vix_sol4_0.040164.mat','ml/mlpr_vix_sol4_0.040362.mat','ml/mlpr_vix_sol4_0.040519.mat','ml/mlpr_vix_sol4_0.041382.mat',};
netfile5 = {'ml/mlpr_vix_sol5_0.026456.mat','ml/mlpr_vix_sol5_0.031563.mat','ml/mlpr_vix_sol5_0.031716.mat','ml/mlpr_vix_sol5_0.033307.mat','ml/mlpr_vix_sol5_0.033406.mat','ml/mlpr_vix_sol5_0.034232.mat',};
netfile6 = {'ml/mlpr_vix_sol6_0.028104.mat','ml/mlpr_vix_sol6_0.030759.mat','ml/mlpr_vix_sol6_0.031087.mat','ml/mlpr_vix_sol6_0.031850.mat','ml/mlpr_vix_sol6_0.033003.mat','ml/mlpr_vix_sol6_0.035602.mat',};
return;

