function [ret, price] = detectsector(ts, offset, duration)
% compute the sector return with weights
% offset is negative, the day before end
% duration is the positive, the duration for return

window = round(duration / 4);
weight = [1, 1, 0, 0];
weight = weight / sum(weight);

for i = [1 : 4]
    closeend = getfield(ts(end + offset - (i - 1) * window), 'CLOSE');
    closeopen = getfield(ts(end + offset - i * window), 'CLOSE');
    wdret(i) = closeend / closeopen - 1;
%     wdclose = getfield(ts(end + offset - i * window + 1 : end + offset - (i - 1) * window), 'CLOSE');
%     wddate = getfield(ts(end + offset - i * window + 1: end + offset - (i - 1) * window), 'dates');
%     wdreg = regress(wdclose, [wddate, ones(window, 1)]);
%     wdret(i) = wdreg(1);
end;

ret = sum(wdret .* weight);
price = getfield(ts(end + offset), 'CLOSE');
