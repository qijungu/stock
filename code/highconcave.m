function [HCON, MHCON] = highconcave(hights)
% LCON is the time seriels of lows that make a concave of segments
% MLCON is the longest low segment

len = length(hights);
high = fts2mat(hights, 1);
x = high(:, 1); % dates
y = high(:, 2); % high
i = 1;
r = 1;
while i < len
    x0 = x(i);
    y0 = y(i);
    hcon(r, :) = [x0, y0];
    j = i + 1;
    while j <= len
        x1 = x(j);
        y1 = y(j);
        k = (y1 - y0) / (x1 - x0);
        concave = true;
        for q = [j + 1 : len]
            if y(q) > k * (x(q) - x0) + y0 % the point of q is above the line
                j = q;
                concave = false;
                break;
            end;
        end;
        if concave
            i = j;
            r = r + 1;
            break;
        end;
    end;
end;
hcon(r, :) = [x(len), y(len)];
HCON = fints(hcon(:, 1), hcon(:, 2), 'HCON');

len = length(HCON);
Mdst = 0;
for i = [1 : len - 1]
    dst = hcon(i + 1, 1) - hcon(i, 1);
    if  dst >= Mdst
        Mdst = dst;
        Mi = i;
    end;
end;
MHCON = fints(hcon(Mi : Mi + 1, 1), hcon(Mi : Mi + 1, 2), 'MHCON');
