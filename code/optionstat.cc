#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include <set>
#include <string>
#include <list>
#include <vector>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>

void toupper(char* str, int size) {
    for (int i = 0; i < size && str[i] != '\0'; i++) str[i] &= 0x5F;
}

int main(int argc, char** argv) {

    if (argc != 2) {
        printf("./optionstat stock\n");
        printf("Stat option pain and stock price.\n");
        printf("stock.max, stock.curr, and stock.dat must present.\n");
        return 1;
    }

    char stock[10];
    strncpy(stock, argv[1], 10);
    toupper(stock, 10);
    char maxpain[25], currentpain[25], stockdata[25];
    sprintf(maxpain, "option/%s.max", stock);
    sprintf(currentpain, "option/%s.curr", stock);
    sprintf(stockdata, "data/%s.dat", stock);

    char buf[1000];
    FILE *fmaxpain, *fcurrpain, *fprice;

    fprice = fopen(stockdata, "r");
    if (fprice == NULL) { printf("cannot open data file %s\n", stockdata); return 1; }
    fgets(buf, 1000, fprice);
    fgets(buf, 1000, fprice);
    int year, month, day;
    unsigned long long vol;
    double open, high, low, price, adjclose;
    sscanf(buf, "%d-%d-%d,%lf,%lf,%lf,%lf,%lld,%lf", &year, &month, &day, &open, &high, &low, &price, &vol, &adjclose);
    fclose(fprice);

    fcurrpain = fopen(currentpain, "r");
    if (fcurrpain == NULL) { printf("cannot open current pain file %s\n", currentpain); return 1; }
    while (fgets(buf, 1000, fcurrpain) && buf[0] != '=') ;
    assert(feof(fcurrpain) == 0);
    double cpain, cpratio;
    sscanf(buf, "====== current pain is at %lf, priced p/c ratio %lf", &cpain, &cpratio);
    double cratio;
    long cpcnt, cccnt, ctotal;
    fgets(buf, 1000, fcurrpain);
    sscanf(buf, "p/c ratio is %lf, put %ld, call %ld, total %ld\n", &cratio, &cpcnt, &cccnt, &ctotal);
    fclose(fcurrpain);

    fmaxpain = fopen(maxpain, "r");
    if (fmaxpain == NULL) { printf("cannot open max pain file %s\n", maxpain); return 1; }
    while (fgets(buf, 1000, fmaxpain) && buf[0] != '=') ;
    assert(feof(fmaxpain) == 0);
    double mpain, mpratio;
    sscanf(buf, "====== max pain is at %lf, priced p/c ratio %lf", &mpain, &mpratio);
    double mratio;
    long mpcnt, mccnt, mtotal;
    fgets(buf, 1000, fmaxpain);
    sscanf(buf, "p/c ratio is %lf, put %ld, call %ld, total %ld\n", &mratio, &mpcnt, &mccnt, &mtotal);
    fclose(fmaxpain);

    printf("%s, %0.2lf, %0.2lf, %0.5lf, %0.2lf, %0.2lf, %ld, %0.2lf, %0.5lf, %0.2lf, %0.2lf, %ld\n", stock, price, cpain, fabs(price - cpain)/price, cratio, cpratio, ctotal, mpain, fabs(price - mpain)/price, mratio, mpratio, mtotal);

    return 0;
}
