function drawindex(varargin)
% drawindex(index, backend, startday, endday)
% backend: 1 no show figure, 0 show figure
% startday: the start day of the chart. -126 or 126 means 126 stock days before today
% endday: the end day of the chart. -1 or 1 means 1 stock day before today
% default: index = 'VIX', backend = 0, startday = 126 or -126, endday = 0;

netfile3 = {}; %201711
netfile4 = {}; %201712
netfile4 = {}; %201712
netfile5 = {}; %201711
netfile6 = {}; %201711
numfeature3 = 11; % number of features
numfeature4 = 13; % number of features
numfeature5 = 21; % number of features
numfeature6 = 23; % number of features

index = upper('vix');
backend = 0;
startday = 252; % 126, half year, 252, 1 year
endday = 0;

argsz = length(varargin);
if argsz >= 1
    index = upper(varargin{1});
end;
if argsz >= 2
    backend = str2num(varargin{2});
end;
if argsz >= 3
    startday = abs(str2num(varargin{3}));
end;
if argsz >= 4
    endday = abs(str2num(varargin{4}));
end;

indexdata = ['data/matlab/', index, '.dat'];
idts = ascii2fts(indexdata, 1, 2, []);
figdata = ['fig/data/', index, '.dat'];
fdts = ascii2fts(figdata, 1, 2, []);

startday = min([startday, length(fdts) - 126]);
if endday >= startday - 5
    fprintf('The startday %d must be at least 5 days before the endday %d\n', startday, endday);
    return;
end;

idts = idts(end - startday : end - endday);
fdts = fdts(end - startday : end - endday);

date = getfield(idts, 'dates');
len = length(idts);

[LCON10, MLCON10] = lowconcave(idts.LOW(end - 10 : end)); % two weeks
[LCON20, MLCON20] = lowconcave(idts.LOW(end - 20 : end)); % one month
[LCON60, MLCON60] = lowconcave(idts.LOW(end - 60 : end)); % three months

[HCON10, MHCON10] = highconcave(idts.HIGH(end - 10 : end)); % two weeks
[HCON20, MHCON20] = highconcave(idts.HIGH(end - 20 : end)); % one month
[HCON60, MHCON60] = highconcave(idts.HIGH(end - 60 : end)); % three months

pCLOSE = idts.CLOSE;
pMOV = [fdts.MOV200, fdts.MOV100, fdts.MOV50];
pBOLL = [fdts.MIDDLE, fdts.UPPER, fdts.LOWER];
pRET =[fdts.RET8, fdts.RET4];
BOLLWINDOW = pCLOSE([end-20, end]);

lwindow = 8;
swindow = 4;
lret = [[idts.dates(end - lwindow + 1), idts.dates(end)]', [getfield(idts(end - lwindow + 1), 'CLOSE'), getfield(idts(end), 'CLOSE')]'];
sret = [[idts.dates(end - swindow + 1), idts.dates(end)]', [getfield(idts(end - swindow + 1), 'CLOSE'), getfield(idts(end), 'CLOSE')]'];

if strcmp(index, 'VIX')
    spxfigdata = 'fig/data/SPX.dat';
    spxfdts = ascii2fts(spxfigdata, 1, 2, []);
    spxfdts = spxfdts(end - startday : end - endday);
    spxlret = spxfdts.RET8;
    spxsret = spxfdts.RET4;
    ldiv = spxlret + fdts.RET8 / 4;
    sdiv = spxsret + fdts.RET4 / 4;
    pDIV = [ldiv, sdiv];
    
    % vix nnet
    swindow = 4; % short window to get vix change rate
    lwindow = 8; % long window to get vix change rate
    delay = 60; % delay

    tsspy = ascii2fts('data/matlab/SPY.dat', 1, 2, []);
    tsspy = tsspy(end - startday - delay*2 : end - endday);
    tsvix = ascii2fts('data/matlab/VIX.dat', 1, 2, []);
    tsvix = tsvix(end - startday - delay*2 : end - endday);

    % p: price, v: volume, c: change, b: bolling, rs: ret short, rl: ret long
    spyp = fts2mat(tsspy, 0, 'CLOSE');
    spyh = fts2mat(tsspy, 0, 'HIGH');
    spyl = fts2mat(tsspy, 0, 'LOW');
    spyo = fts2mat(tsspy, 0, 'OPEN');
    spyv = fts2mat(tsspy, 0, 'VOLUME');
    spyc = spyp(2:end) ./ spyp(1:end-1) - 1;
    [spymid, spyup, spylow] = bollinger(tsspy);
    spyb = fts2mat(spyup.CLOSE) - fts2mat(spylow.CLOSE);
    spyup=fts2mat(spyup, 'CLOSE');
    spylow=fts2mat(spylow, 'CLOSE');
    spyrs = spyp(swindow+1:end)./spyp(swindow/2+1:end-swindow/2) + spyp(swindow/2+1:end-swindow/2)./spyp(1:end-swindow);
    spyrl = spyp(lwindow+1:end)./spyp(lwindow/2+1:end-lwindow/2) + spyp(lwindow/2+1:end-lwindow/2)./spyp(1:end-lwindow);
    [vixmid, vixup, vixlow] = bollinger(tsvix);
    vixb = fts2mat(vixup.CLOSE) - fts2mat(vixlow.CLOSE);
    vixp = fts2mat(tsvix, 0, 'CLOSE');
    vixh = fts2mat(tsvix, 0, 'HIGH');
    vixl = fts2mat(tsvix, 0, 'LOW');
    vixo = fts2mat(tsvix, 0, 'OPEN');
    vixc = vixp(2:end) ./ vixp(1:end-1) - 1;
    vixpn = (vixp - fts2mat(vixmid.CLOSE)) ./ vixb; % normalized vixp
    vixcn = vixpn(2:end) - vixpn(1:end-1); % normalized vixp change rate
    vixup=fts2mat(vixup, 'CLOSE');
    vixlow=fts2mat(vixlow, 'CLOSE');
    vixrs = vixp(swindow+1:end)./vixp(swindow/2+1:end-swindow/2) + vixp(swindow/2+1:end-swindow/2)./vixp(1:end-swindow);
    vixrl = vixp(lwindow+1:end)./vixp(lwindow/2+1:end-lwindow/2) + vixp(lwindow/2+1:end-lwindow/2)./vixp(1:end-lwindow);

    inputs3 = zeros(numfeature3*delay, len);
    for i=0:delay-1
        inputs3(i*numfeature3+1:(i+1)*numfeature3,:)=[
        spyp(end-i-len+1:end-i)';
        spyv(end-i-len+1:end-i)';
        spyc(end-i-len+1:end-i)';
        spyb(end-i-len+1:end-i)';
        vixp(end-i-len+1:end-i)';
        vixc(end-i-len+1:end-i)';
        vixb(end-i-len+1:end-i)';
        vixup(end-i-len+1:end-i)';
        vixlow(end-i-len+1:end-i)';
        vixrs(end-i-len+1:end-i)';
        vixrl(end-i-len+1:end-i)';
        ];
    end;

    tsnet3 = {};
    for i=1:length(netfile3)
        nf3 = netfile3{i};
        net3 = load(nf3);
        net3 = net3.net;
        outputs3 = net3(inputs3);
        a=tsvix(end-len+1:end);
        tsnet3{i} = fints(a.dates, outputs3',{'RiseFall3'});
    end;

    inputs4 = zeros(numfeature4*delay, len);
    for i=0:delay-1
        inputs4(i*numfeature4+1:(i+1)*numfeature4,:)=[
        spyp(end-i-len+1:end-i)';
        spyv(end-i-len+1:end-i)';
        spyc(end-i-len+1:end-i)';
        spyb(end-i-len+1:end-i)';
        vixp(end-i-len+1:end-i)';
        vixc(end-i-len+1:end-i)';
        vixpn(end-i-len+1:end-i)';
        vixcn(end-i-len+1:end-i)';
        vixb(end-i-len+1:end-i)';
        vixup(end-i-len+1:end-i)';
        vixlow(end-i-len+1:end-i)';
        vixrs(end-i-len+1:end-i)';
        vixrl(end-i-len+1:end-i)';
        ];
    end;

    tsnet4 = {};
    for i=1:length(netfile4)
        nf4 = netfile4{i};
        net4 = load(nf4);
        net4 = net4.net;
        outputs4 = net4(inputs4);
        a=tsvix(end-len+1:end);
        tsnet4{i} = fints(a.dates, outputs4',{'RiseFall4'});
    end;

    inputs5 = zeros(numfeature5*delay, len);
    for i=0:delay-1
        inputs5(i*numfeature5+1:(i+1)*numfeature5,:)=[
	spyp(end-i-len+1:end-i)';
	spyh(end-i-len+1:end-i)';
	spyl(end-i-len+1:end-i)';
	spyo(end-i-len+1:end-i)';
	spyv(end-i-len+1:end-i)';
	spyc(end-i-len+1:end-i)';
	spyb(end-i-len+1:end-i)';
	spyup(end-i-len+1:end-i)';
	spylow(end-i-len+1:end-i)';
	spyrs(end-i-len+1:end-i)';
	spyrl(end-i-len+1:end-i)';
	vixp(end-i-len+1:end-i)';
	vixh(end-i-len+1:end-i)';
	vixl(end-i-len+1:end-i)';
	vixo(end-i-len+1:end-i)';
	vixc(end-i-len+1:end-i)';
	vixb(end-i-len+1:end-i)';
	vixup(end-i-len+1:end-i)';
	vixlow(end-i-len+1:end-i)';
	vixrs(end-i-len+1:end-i)';
	vixrl(end-i-len+1:end-i)';
        ];
    end;

    tsnet5 = {};
    for i=1:length(netfile5)
        nf5 = netfile5{i};
        net5 = load(nf5);
        net5 = net5.net;
        outputs5 = net5(inputs5);
        a=tsvix(end-len+1:end);
        tsnet5{i} = fints(a.dates, outputs5',{'RiseFall5'});
    end;

    inputs6 = zeros(numfeature6*delay, len);
    for i=0:delay-1
        inputs6(i*numfeature6+1:(i+1)*numfeature6,:)=[
	spyp(end-i-len+1:end-i)';
	spyh(end-i-len+1:end-i)';
	spyl(end-i-len+1:end-i)';
	spyo(end-i-len+1:end-i)';
	spyv(end-i-len+1:end-i)';
	spyc(end-i-len+1:end-i)';
	spyb(end-i-len+1:end-i)';
	spyup(end-i-len+1:end-i)';
	spylow(end-i-len+1:end-i)';
	spyrs(end-i-len+1:end-i)';
	spyrl(end-i-len+1:end-i)';
	vixp(end-i-len+1:end-i)';
	vixh(end-i-len+1:end-i)';
	vixl(end-i-len+1:end-i)';
	vixo(end-i-len+1:end-i)';
	vixc(end-i-len+1:end-i)';
	vixpn(end-i-len+1:end-i)';
	vixcn(end-i-len+1:end-i)';
	vixb(end-i-len+1:end-i)';
	vixup(end-i-len+1:end-i)';
	vixlow(end-i-len+1:end-i)';
	vixrs(end-i-len+1:end-i)';
	vixrl(end-i-len+1:end-i)';
        ];
    end;

    tsnet6 = {};
    for i=1:length(netfile6)
        nf6 = netfile6{i};
        net6 = load(nf6);
        net6 = net6.net;
        outputs6 = net6(inputs6);
        a=tsvix(end-len+1:end);
        tsnet6{i} = fints(a.dates, outputs6',{'RiseFall6'});
    end;

end;

if strcmp(index, 'VDAX')
    daxfigdata = 'fig/data/DAX.dat';
    daxfdts = ascii2fts(daxfigdata, 1, 2, []);
    daxfdts = daxfdts(end - startday : end - endday);
    daxlret = daxfdts.RET8;
    daxsret = daxfdts.RET4;
    ldiv = daxlret + fdts.RET8 / 4;
    sdiv = daxsret + fdts.RET4 / 4;
    pDIV = [ldiv, sdiv];
end;

if strcmp(index, 'RVX')
    rutfigdata = 'fig/data/RUT.dat';
    rutfdts = ascii2fts(rutfigdata, 1, 2, []);
    rutfdts = rutfdts(end - startday : end - endday);
    rutlret = rutfdts.RET8;
    rutsret = rutfdts.RET4;
    ldiv = rutlret + fdts.RET8 / 4;
    sdiv = rutsret + fdts.RET4 / 4;
    pDIV = [ldiv, sdiv];
end;

figure(1); close(1);
fh =figure(1);
if backend
    set(fh, 'Visible', 'off');
end;

subplot(2, 1, 1);
hold on;
h = candle(idts);
for i = 1:size(h,1)
    set(get(get(h(i),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
end
h = plot(pBOLL, 'k');
set(get(get(h(1),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(2),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
set(get(get(h(3),'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MLCON10);
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MLCON20);
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MLCON60);
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MHCON10);
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MHCON20);
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(MHCON60);
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(BOLLWINDOW);
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(lret(:, 1), lret(:, 2), '-m');
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
h = plot(sret(:, 1), sret(:, 2), '-m');
set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
yl = ylim;
yl = [0, yl(2)];
plot(pMOV);
%ylim(yl);
if strcmp(index, 'VIX') || strcmp(index, 'GVZ') || strcmp(index, 'VXFXI') || strcmp(index, 'VXGDX') || strcmp(index, 'VXSLV') || strcmp(index, 'VXXLE') || strcmp(index, 'VXTYN') || strcmp(index, 'VDAX') || strcmp(index, 'RVX')
    pBOLLDELTA = fints(pBOLL.dates, fts2mat(pBOLL.UPPER) - fts2mat(pBOLL.LOWER));
    h = plot(pBOLLDELTA, '-+m');
end;
if strcmp(index, 'VIX')
    ylim([0,50]);
    yl = ylim;
    patterns = {'-ob','-og','-or','-oc','-om','-oy','-ok'};
    for i=1:length(tsnet3)
        h = plot(tsnet3{i}*yl(2), patterns{i});
    end;
    patterns = {'-xb','-xg','-xr','-xc','-xm','-xy','-xk'};
    for i=1:length(tsnet4)
        h = plot(tsnet4{i}*yl(2), patterns{i});
    end;
    patterns = {'-+b','-+g','-+r','-+c','-+m','-+y','-+k'};
    for i=1:length(tsnet5)
        h = plot(tsnet5{i}*yl(2), patterns{i});
    end;
    patterns = {'-sb','-sg','-sr','-sc','-sm','-sy','-sk'};
    for i=1:length(tsnet6)
        h = plot(tsnet6{i}*yl(2), patterns{i});
    end;
end;
grid minor;
xl = xlim;
xd = (xl(2) - xl(1)) / 12;
xtick = [xl(1) : xd : xl(2)];
xlabel = datestr(xtick, 23);
set(gca, 'XTick', xtick);
set(gca, 'XTickLabel', xlabel);
if strcmp(index, 'VIX')
    lds = {'MOV200', 'MOV100', 'MOV50', 'BollDelta'};
    for i=1:length(tsnet3)
        lds{4+i} = ['RiseFall3.',num2str(i)];
    end;
    for i=1:length(tsnet4)
        lds{4+length(tsnet3)+i} = ['RiseFall4.',num2str(i)];
    end;
    for i=1:length(tsnet5)
        lds{4+length(tsnet3)+length(tsnet4)+i} = ['RiseFall5.',num2str(i)];
    end;
    for i=1:length(tsnet6)
        lds{4+length(tsnet3)+length(tsnet4)+length(tsnet5)+i} = ['RiseFall6.',num2str(i)];
    end;
    legend(lds);
elseif strcmp(index, 'GVZ') || strcmp(index, 'VXFXI') || strcmp(index, 'VXGDX') || strcmp(index, 'VXSLV') || strcmp(index, 'VXXLE') || strcmp(index, 'VXTYN') || strcmp(index, 'VDAX') || strcmp(index, 'RVX')
    legend({'MOV200', 'MOV100', 'MOV50', 'BollDelta'});
else
    legend({'MOV200', 'MOV100', 'MOV50'});
end;

legend('Location','North');
set(gca, 'YAxisLocation', 'right');
ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];

subplot(2, 1, 2);
hold on;
if strcmp(index, 'VIX') || strcmp(index, 'VDAX') || strcmp(index, 'RVX')
    h = plot([pRET, pDIV], '-+');
    set(h, 'MarkerSize', 4);
    ylim([-0.20, 0.30]);
    grid minor;
    xl = xlim;
    h = plot(xlim, [0, 0], '-r');
    set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
    xd = (xl(2) - xl(1)) / 12;
    xtick = [xl(1) : xd : xl(2)];
    xlabel = datestr(xtick, 23);
    set(gca, 'XTick', xtick);
    set(gca, 'XTickLabel', xlabel);
    legend({'RET8', 'RET4', 'DIV8', 'DIV4'});
else
    h = plot([pRET], '-+');
    set(h, 'MarkerSize', 4);
    ylim([-0.05, 0.03]);
    grid minor;
    xl = xlim;
    h = plot(xlim, [0, 0], '-r');
    set(get(get(h,'Annotation'),'LegendInformation'), 'IconDisplayStyle', 'off');
    xd = (xl(2) - xl(1)) / 12;
    xtick = [xl(1) : xd : xl(2)];
    xlabel = datestr(xtick, 23);
    set(gca, 'XTick', xtick);
    set(gca, 'XTickLabel', xlabel);
    legend({'RET8', 'RET4'});
end;
legend('Location','SouthWest');
title(index);
set(gca, 'YAxisLocation', 'right');

ax = gca;
outerpos = ax.OuterPosition;
ti = ax.TightInset; 
left = outerpos(1) + ti(1);
bottom = outerpos(2) + ti(2);
ax_width = outerpos(3) - ti(1) - ti(3);
ax_height = outerpos(4) - ti(2) - ti(4);
ax.Position = [left bottom ax_width ax_height];
set(fh, 'PaperSize',[22,17], 'PaperPositionMode','auto', 'PaperPosition',[-1,-1,24,18.5]);
print(['fig/', index, '.eps'], '-dpsc2')
if backend
    close(fh);
end;

if strcmp(index, 'VIX')
    drawvixnnet;
end;
