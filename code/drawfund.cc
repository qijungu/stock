#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char** argv) {
    char mf[10];
    char cmd[100];
    if (argc == 1) {
        printf("./drawfund all [startday [endday]]\n");
        printf("./drawfund mf [startday [endday]]\n");
        printf("\n");
        return 1;
    }
    int startday = 252;
    int endday = 0;
    if (argc >= 3) startday = atoi(argv[2]);
    if (argc >= 4) endday = atoi(argv[3]);
    if (strncmp(argv[1], "all", 3) == 0) {
        printf("Drawing all mutual funds\n\n");
        FILE* fp = fopen("mf.list", "r");
        while(fscanf(fp, "%s", mf)) {
            if (feof(fp)) break; // reach the end of mf list
            if (mf[0] == '#') continue; // comment out
            printf("Drawing %s\n", mf);
            //sprintf(cmd, "./rundrawmf.sh $MATLAB %s 1 %d %d", mf, startday, endday);
            sprintf(cmd, "matlab 1>/dev/null -nosplash -nodesktop -r \"drawmf(\'%s\', \'1\', \'%d\', \'%d\'); exit\"", mf, startday, endday);
            if (system(cmd) < 0) break;
        }
        fclose(fp);
    } else {
        strncpy(mf, argv[1], 10);
        //sprintf(cmd, "./rundrawmf.sh $MATLAB %s 0 %d %d", mf, startday, endday);
        sprintf(cmd, "matlab 1>/dev/null -nosplash -nodesktop -r \"drawmf(\'%s\', \'1\', \'%d\', \'%d\'); exit\"", mf, startday, endday);
        system(cmd);
    }
    printf("\n");
    return 0;
}
