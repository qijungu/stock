function detect

dlist = dir('detect.record');
detdate = 0;
for i = [1: length(dlist)]
    if ~isempty(strfind(dlist(i).name, 'detect'))
        [~, remain] = strtok(dlist(i).name, '-');
        [year, remain] = strtok(remain, '-');
        [month, remain] = strtok(remain, '-');
        day = strtok(remain, '-.');
        dt = datenum(str2num(year), str2num(month), str2num(day));
        if dt > detdate
            detdate = dt;
        end;
    end;
end;

spydata = 'data/matlab/SPY.dat';
spyts = ascii2fts(spydata, 1, 2, []);

if detdate == spyts.dates(end)
    return;
end;

if detdate == 0
    duration = 1;
else
    duration = 0;
    while spyts.dates(end - duration) > detdate
        duration = duration + 1;
    end;
end;

for drt = [1 : duration]
    
    clear dtstr stockbuy stocksell sectorbuy

    dt = spyts.dates(end - duration + drt);
    detfile = ['detect.record/detect-', datestr(dt, 'yyyy-mm-dd'), '.txt'];
    detfp = fopen(detfile, 'w');
    
    dtstr{1} = sprintf('==================================================');
    dtstr{2} = sprintf('=================== %s ===================', datestr(dt, 'yyyy-mm-dd'));
%     dtstr(:)
    for i = [1: length(dtstr)]
        fprintf(detfp, '%s\n', dtstr{i});
    end;
    fprintf(detfp, '\n');
   
    % long term signals
    
    fp = fopen('stock.list', 'r');
    i = 1;
    while ~feof(fp)
        stock = fscanf(fp, '%s^[\n]');
        if isempty(stock)
            continue;
        end;
        stocklist{i} = stock;
        i = i + 1;
    end;
    fclose(fp);
    len = length(stocklist);
    
    stockbuy{1} = sprintf('====== Buy signal (long term) ======');
    stockbuy{2} = sprintf('        :\tBuy\tSTBE\tSTBU\tLTBE\tLTBU\tSTI');
    jb = 3;
    
    stocksell{1} = sprintf('====== Sell signal (long term) ======');
    stocksell{2} = sprintf('        :\tSell\tSTBE\tSTBU\tLTBE\tLTBU\tSTI');
    js = 3;
    
    for i = [1 : len]
        stockdata = ['data/matlab/', stocklist{i}, '.dat'];
        sdts = ascii2fts(stockdata, 1, 2, []);
        figdata = ['fig/data/', stocklist{i}, '.dat'];
        fdts = ascii2fts(figdata, 1, 2, []);
        fdts = fdts(1 : end - duration + drt);
        
        buy = getfield(fdts(end), 'BUY');
        sell = getfield(fdts(end), 'SELL');
        stbe = getfield(fdts(end), 'STBE');
        stbu = getfield(fdts(end), 'STBU');
        ltbe = getfield(fdts(end), 'LTBE');
        ltbu = getfield(fdts(end), 'LTBU');
        senti = getfield(fdts(end), 'SENTI');
        
        if buy > 0
            stockbuy{jb} = sprintf('%-8s:\t%0.1f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f', stocklist{i}, buy, stbe, stbu, ltbe, ltbu, senti);
            analyze(stocklist{i}, 1);
            jb = jb + 1;
        end;
        
        if sell > 0
            senti = getfield(fdts(end), 'SENTI');
            stocksell{js} = sprintf('%-8s:\t%0.1f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f', stocklist{i}, sell, stbe, stbu, ltbe, ltbu, senti);
            js = js + 1;
        end;
    end;
    
%     stockbuy(:)
%     stocksell(:)
    for i = [1: length(stockbuy)]
        fprintf(detfp, '%s\n', stockbuy{i});
    end;
    fprintf(detfp, '\n');
    for i = [1: length(stocksell)]
        fprintf(detfp, '%s\n', stocksell{i});
    end;
    fprintf(detfp, '\n');
    
    % % short term signals
    %
    % shortbuy{1} = sprintf('====== Buy signal (short term) ======');
    % [buy, buyprice, stopdelta] = detectshort('spx');
    % if buy
    %     shortbuy{2} = sprintf('1. Buy SPX at the max of high %8.2f and tomorrow open (?).', buyprice);
    %     shortbuy{3} = sprintf('2. Set stop loss at buy price - stopdelta (%6.2f).', stopdelta);
    %     shortbuy{4} = sprintf('3. Sell at the first profitable open or stop loss after tomorrow.');
    % end;
    % shortbuy(:)
    
    % mutal fund sector signals
    
    fp = fopen('mf.list', 'r');
    i = 1;
    while ~feof(fp)
        sector = fscanf(fp, '%s^[\n]');
        if isempty(sector)
            continue;
        end;
        sectorlist{i} = sector;
        i = i + 1;
    end;
    fclose(fp);
    
    rotwindow = 30;
    perfwindow = 120;
    testsz = 3;
    numpicks = 5;
    seclen = length(sectorlist);
    ret = zeros(seclen, testsz);
    price = zeros(seclen, testsz);
    
    for i = [1 : seclen]
        sector = sectorlist{i};
        ts = ascii2fts(['data/matlab/', sector, '.dat'], 1, 2, []);
        ts = ts(1 : end - duration + drt);
        for j = [1 : testsz]
            [ret(i, j), price(i, j)] = detectsector(ts, -(testsz - j) * rotwindow, perfwindow);
        end;
    end;
    
    goodpicks = zeros(testsz, numpicks);
    badpicks = zeros(testsz, numpicks);
    for i = [1 : testsz]
        [~, p] = sort(ret(:, i), 'descend');
        goodpicks(i, :) = p(1 : numpicks)';
        badpicks(i, :) = p(seclen - numpicks + 1 : seclen);
    end;
    
    sectorbuy{1} = sprintf('====== The best and worst %d sectors (long term) ======', numpicks);
    sectorbuy{2} = sprintf('Picks-rnd\tOrder\tName\tReturn\tOrder\tName\tReturn');
    idx = 3;
    for i = [1 : testsz]
        sectorbuy{idx} = '------';
        idx = idx + 1;
        for j = [1 : numpicks]
            sectorbuy{idx} = sprintf('Picks-%d\t%3d\t%s\t%6.3f\t%3d\t%s\t%6.3f', testsz - i, j, sectorlist{goodpicks(i, j)}, ret(goodpicks(i, j), i), seclen - numpicks + j, sectorlist{badpicks(i, j)}, ret(badpicks(i, j), i));
            idx = idx + 1;
        end;
    end;
%     sectorbuy(:)
    for i = [1: length(sectorbuy)]
        fprintf(detfp, '%s\n', sectorbuy{i});
    end;
    fprintf(detfp, '\n');
    
    perfwindow = 60;
    numpicks = 5;
    seclen = length(sectorlist);
    ret = zeros(seclen, 1);
    price = zeros(seclen, 1);
    
    for i = [1 : seclen]
        sector = sectorlist{i};
        ts = ascii2fts(['data/matlab/', sector, '.dat'], 1, 2, []);
        ts = ts(1 : end - duration + drt);
        [ret(i), price(i)] = detectsector(ts, 0, perfwindow);
    end;
    
    [~, p] = sort(ret, 'descend');
    goodpicks = p(1 : numpicks);
    badpicks = p(seclen - numpicks + 1 : seclen);
    
    clear sectorbuy
    sectorbuy{1} = sprintf('====== The best and worst %d sectors (short term) ======', numpicks);
    sectorbuy{2} = sprintf('Picks-rnd\tOrder\tName\tReturn\tOrder\tName\tReturn');
    idx = 3;
    for i = [1 : numpicks]
        sectorbuy{idx} = sprintf('Picks-%d\t%3d\t%s\t%6.3f\t%3d\t%s\t%6.3f', 0, i, sectorlist{goodpicks(i)}, ret(goodpicks(i)), seclen - numpicks + i, sectorlist{badpicks(i)}, ret(badpicks(i)));
        idx = idx + 1;
    end;
%     sectorbuy(:)
    for i = [1: length(sectorbuy)]
        fprintf(detfp, '%s\n', sectorbuy{i});
    end;
    fprintf(detfp, '\n');

    fclose(detfp);
    
end;
