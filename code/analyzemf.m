function analyzemf(varargin)
% analyze(mf)
% default: 'FBIOX', 180

argsz = size(varargin, 2);
mf = upper('fbiox');
duration = 1350; % 252, 1 year

if argsz == 1
    mf = upper(varargin{1});
end;

mfdata = ['data/matlab/', mf, '.dat'];
ts = ascii2fts(mfdata, 1, 2, []);
duration = min([duration, length(ts) - 252]);

if duration < 0
    sprintf('No analysis on %s for less than 1 year\n', mf);
    return;
end;

mdts = ts(end - duration - 250 : end);

figdata = ['fig/data/', mf, '.dat'];
if exist(figdata, 'file')
    prevfdts = ascii2fts(figdata, 1, 2, []);
    prevfdts = prevfdts(1 : end - 2);
    duration = 0;
    while mdts.dates(end - duration) ~= prevfdts.dates(end)
        duration = duration + 1;
    end;
end;

if duration == 0
    return; % no update
end;

[mid, up, low] = bollinger(mdts);
mid = chfield(mid, 'CLOSE', 'MIDDLE');
up = chfield(up, 'CLOSE', 'UPPER');
low = chfield(low, 'CLOSE', 'LOWER');
BOLL = [mid.MIDDLE, up.UPPER, low.LOWER];
pBOLL = BOLL(end - duration + 1 : end);

mov200 = tsmovavg(mdts.CLOSE, 's', 200);
mov200 = chfield(mov200, 'CLOSE', 'MOV200');
mov100 = tsmovavg(mdts.CLOSE, 's', 100);
mov100 = chfield(mov100, 'CLOSE', 'MOV100');
mov50 = tsmovavg(mdts.CLOSE, 's', 50);
mov50 = chfield(mov50, 'CLOSE', 'MOV50');
MOV = merge(mov200, mov100, mov50, 'SortColumns', false);
pMOV = MOV(end - duration + 1 : end);

lwindow = 60 * 2;
swindow = 30 * 2;
for i = [1 : duration]
    lret(i) = detectsector(ts(end - duration + i - lwindow : end - duration + i), 0, lwindow);
    sret(i) = detectsector(ts(end - duration + i - swindow : end - duration + i), 0, swindow);
end;
pRET = fints(pBOLL.dates, [lret', sret'], {'RET60', 'RET30'});

fdts = [pMOV, pBOLL, pRET];
if exist(figdata, 'file')
    fdts = merge(fdts, prevfdts, 'SortColumns', false);
end;

date = getfield(fdts, 'dates');
fdmat = fts2mat(fdts);
len = length(fdts);

fp = fopen(figdata, 'w');
fprintf(fp, '%s\n', mf);
fprintf(fp, 'DATE\tMOV200\tMOV100\tMOV50\tMIDDLE\tUPPER\tLOWER\tRET60\tRET30\n');
for i = [len : -1 : 1]
    fprintf(fp, '%s\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\n', datestr(date(i), 23), fdmat(i, :));
end;
fclose(fp);
