function [buy, buyprice, stopdelta] = detectshort(stock)

stock = upper(stock);
stockdata = ['data/matlab/', stock, '.dat'];
ts = ascii2fts(stockdata, 1, 2, []);
len = length(ts);
window = 10;
lossmargin = 0.5;
ts = ts(end - window + 1 : end);
wr = willpctr(ts, window);

% rule 7, open/clos with bailout || rule 8, %r with bailout
if (getfield(ts(end), 'CLOSE') < getfield(ts(end - 1), 'CLOSE') && getfield(ts(end), 'CLOSE') > getfield(ts(end), 'OPEN')) || (getfield(ts(end), 'CLOSE') < getfield(ts(end - 1), 'LOW') && 100 + getfield(wr(end), 'WillPctR') < 20) && weekday(ts.dates(end)) ~= 5
    buy = true;
    buyprice = getfield(ts(end), 'HIGH'); % buy at the max of today's high and tomorrow's open.
    % sell at the first gain open or stop loss
else
    buy = false;
    buyprice = getfield(ts(end), 'CLOSE');
end;

vlth = getfield(ts, 'HIGH');
vltl = getfield(ts, 'LOW');
vltrng = vlth - vltl;
vlt = median(vltrng);
stopdelta = vlt * lossmargin;

