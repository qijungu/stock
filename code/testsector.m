function testsector()

fp = fopen('mf.list', 'r');
i = 1;
while ~feof(fp)
    sector = fscanf(fp, '%s^[\n]');
    if isempty(sector) 
        continue;
    end;
    sectorlist{i} = sector;
    i = i + 1;
end;
fclose(fp);

numpicks = 3;
rotwindow = 30;
perfwindow = 120;
testsz = floor(1000 / rotwindow);
seclen = length(sectorlist);
ret = zeros(seclen, testsz);
price = zeros(seclen, testsz);
spx = zeros(1, testsz);

spxts = ascii2fts(['data/matlab/SPX.dat'], 1, 2, []);
for i = [1 : seclen]
    sector = sectorlist{i};
    ts = ascii2fts(['data/matlab/', sector, '.dat'], 1, 2, []);
    for j = [1 : testsz]
        [ret(i, j), price(i, j)] = detectsector(ts, -(testsz - j) * rotwindow, perfwindow);
        spx(j) = getfield(spxts(end - (testsz - j) * rotwindow), 'CLOSE');
    end;
end;

[~, retidx] = sort(ret(:, 1), 'descend');
picks = [retidx(1 : 2)', 0];
gain = [1, 1, 1];
for i = [2: testsz]
    [~, retidx] = sort(ret(:, i), 'descend');
    prevpicks = picks;
    newpicks = retidx(1 : numpicks)';
    if sum(newpicks(1 : 3) == picks(1)) > 0 % still in the list
        newpicks(find(newpicks == picks(1))) = [];
    else % not in the list
        picks(1) = -1;
    end;
    if sum(newpicks(1 : min(length(newpicks), 3)) == picks(2)) > 0 % still in the list
        newpicks(find(newpicks == picks(2))) = [];
    else % not in the list
        picks(2) = -1;
    end;
    
% strategy 1, pick the best two from three
    if picks(1) < 0
        picks(1) = newpicks(1);
        newpicks(1) = [];
    end;
    if picks(2) < 0
        picks(2) = newpicks(1);
        newpicks(1) = [];
    end;
    
% strategy 2, replace with the fast rising oen 
%     if picks(1) < 0
%         deltaret = ret(newpicks, i) - ret(newpicks, i - 1);
%         [~, idx] = sort(deltaret, 'descend');
%         picks(1) = newpicks(idx(1));
%         newpicks(idx(1)) = [];
%     end;
%     if picks(2) < 0
%         deltaret = ret(newpicks, i) - ret(newpicks, i - 1);
%         [~, idx] = sort(deltaret, 'descend');
%         picks(2) = newpicks(idx(1));
%         newpicks(idx(1)) = [];
%     end;


% strategy 3, similar to strategy 1, but pick the best two from three only if both have positive ret in the past.
%     if picks(1) < 0
%         picks(1) = newpicks(1);
%         newpicks(1) = [];
%     end;
%     if picks(2) < 0
%         picks(2) = newpicks(1);
%         newpicks(1) = [];
%     end;
%     if (ret(picks(1), i) < 0 || ret(picks(2), i) < 0)
%         picks(1) = -1;
%         picks(2) = -1;
%     end;
    
    if prevpicks(1) < 0
        gain(1) = gain(1);
    else
        gain(1) = gain(1) * price(prevpicks(1), i) / price(prevpicks(1), i - 1);
    end;
    if prevpicks(2) < 0
        gain(2) = gain(2);
    else
        gain(2) = gain(2) * price(prevpicks(2), i) / price(prevpicks(2), i - 1);
    end;
    gain(3) = gain(3) * spx(i) / spx(i - 1);
    
    gain = [sum(gain(1:2))/2, sum(gain(1:2))/2, gain(3)];
    [picks; gain]
end;
