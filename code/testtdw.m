function testtdw

stockdata = ['data/matlab/SPY.dat'];
ts = ascii2fts(stockdata, 1, 2, []);
date = getfield(ts, 'dates');
close = getfield(ts, 'CLOSE');
len = length(ts);
dwk = weekday(date);

monday = zeros(2, length(find(dwk == 2)));
lenmon = 0;
tuesday = zeros(2, length(find(dwk == 3)));
lentue = 0;
wednesday = zeros(2, length(find(dwk == 4)));
lenwed = 0;
thursday = zeros(2, length(find(dwk == 5)));
lenthu = 0;
friday = zeros(2, length(find(dwk == 6)));
lenfri = 0;

for i = [2 : len]
    switch dwk(i)
        case 2,
            lenmon = lenmon + 1;
            monday(1, lenmon) = close(i) - close(i - 1);
            monday(2, lenmon) = monday(1, lenmon) / close(i - 1);
        case 3,
            lentue = lentue + 1;
            tuesday(1, lentue) = close(i) - close(i - 1);
            tuesday(2, lentue) = tuesday(1, lentue) / close(i - 1);
        case 4,
            lenwed = lenwed + 1;
            wednesday(1, lenwed) = close(i) - close(i - 1);
            wednesday(2, lenwed) = wednesday(1, lenwed) / close(i - 1);
        case 5,
            lenthu = lenthu + 1;
            thursday(1, lenthu) = close(i) - close(i - 1);
            thursday(2, lenthu) = thursday(1, lenthu) / close(i - 1);
        case 6,
            lenfri = lenfri + 1;
            friday(1, lenfri) = close(i) - close(i - 1);
            friday(2, lenfri) = friday(1, lenfri) / close(i - 1);
    end;
end;

monpcnt = length(find(monday(1, :) > 0)) / lenmon; % positive day count
monpchange = median(monday(2, find(monday(2, :) > 0))); % positive change
monncnt = length(find(monday(1, :) < 0)) / lenmon; % negative day count
monnchange = median(monday(2, find(monday(2, :) < 0))); % negative change
mondiff = (monpcnt * monpchange + monncnt * monnchange) * 1000;

tuepcnt = length(find(tuesday(1, :) > 0)) / lentue; % positive day count
tuepchange = median(tuesday(2, find(tuesday(2, :) > 0))); % positive change
tuencnt = length(find(tuesday(1, :) < 0)) / lentue; % negative day count
tuenchange = median(tuesday(2, find(tuesday(2, :) < 0))); % negative change
tuediff = (tuepcnt * tuepchange + tuencnt * tuenchange) * 1000;

wedpcnt = length(find(wednesday(1, :) > 0)) / lenwed; % positive day count
wedpchange = median(wednesday(2, find(wednesday(2, :) > 0))); % positive change
wedncnt = length(find(wednesday(1, :) < 0)) / lenwed; % negative day count
wednchange = median(wednesday(2, find(wednesday(2, :) < 0))); % negative change
weddiff = (wedpcnt * wedpchange + wedncnt * wednchange) * 1000;

thupcnt = length(find(thursday(1, :) > 0)) / lenthu; % positive day count
thupchange = median(thursday(2, find(thursday(2, :) > 0))); % positive change
thuncnt = length(find(thursday(1, :) < 0)) / lenthu; % negative day count
thunchange = median(thursday(2, find(thursday(2, :) < 0))); % negative change
thudiff = (thupcnt * thupchange + thuncnt * thunchange) * 1000;

fripcnt = length(find(friday(1, :) > 0)) / lenfri; % positive day count
fripchange = median(friday(2, find(friday(2, :) > 0))); % positive change
frincnt = length(find(friday(1, :) < 0)) / lenfri; % negative day count
frinchange = median(friday(2, find(friday(2, :) < 0))); % negative change
fridiff = (fripcnt * fripchange + frincnt * frinchange) * 1000;

'====== down and up days ======'
'[0, +1, +median, -1, -median, diff]'
[1, monpcnt, monpchange, monncnt, monnchange, mondiff]
[2, tuepcnt, tuepchange, tuencnt, tuenchange, tuediff]
[3, wedpcnt, wedpchange, wedncnt, wednchange, weddiff]
[4, thupcnt, thupchange, thuncnt, thunchange, thudiff]
[5, fripcnt, fripchange, frincnt, frinchange, fridiff]
