function analyzestock(varargin)
% analyzestock(stock)
% default: 'SPY'

argsz = size(varargin, 2);
stock = upper('spy');
duration = 3000; % 252, 1 year

if argsz == 1
    stock = upper(varargin{1});
end;

stockdata = ['data/matlab/', stock, '.dat'];
ts = ascii2fts(stockdata, 1, 2, []);
duration = min([duration, length(ts) - 252]);

if duration < 0
    sprintf('No analysis on %s for less than 1 year\n', stock);
    return;
end;

sdts = ts(end - duration - 250 : end);

figdata = ['fig/data/', stock, '.dat'];
if exist(figdata, 'file')
    prevfdts = ascii2fts(figdata, 1, 2, []);
    prevfdts = prevfdts(1 : end - 2);
    duration = 0;
    while sdts.dates(end - duration) ~= prevfdts.dates(end)
        duration = duration + 1;
    end;
end;

if duration == 0
    return; % no update
end;

[mid, up, low] = bollinger(sdts);
mid = chfield(mid, 'CLOSE', 'MIDDLE');
up = chfield(up, 'CLOSE', 'UPPER');
low = chfield(low, 'CLOSE', 'LOWER');
BOLL = [mid.MIDDLE, up.UPPER, low.LOWER];
pBOLL = BOLL(end - duration + 1 : end);

mov200 = tsmovavg(sdts.CLOSE, 's', 200);
mov200 = chfield(mov200, 'CLOSE', 'MOV200');
mov100 = tsmovavg(sdts.CLOSE, 's', 100);
mov100 = chfield(mov100, 'CLOSE', 'MOV100');
mov50 = tsmovavg(sdts.CLOSE, 's', 50);
mov50 = chfield(mov50, 'CLOSE', 'MOV50');
MOV = merge(mov200, mov100, mov50, 'SortColumns', false);
pMOV = MOV(end - duration + 1 : end);

% WADV and TT cannot use sdts. They can only use ts, because they are cummulative.
WADV = willadvol(ts); % rule 6 with volume
pWADV = WADV(end - duration  + 1: end);
TT = truetrend(ts);
pTT = TT(end - duration  + 1: end);

for i = [1 : duration]
    div(i, :) = fts2mat(detectstock(sdts, - (duration - i), 1));
end;
pDIV = fints(pBOLL.dates, div, {'STBE', 'STBU', 'LTBE', 'LTBU', 'SENTI', 'PDIV'});

if exist(figdata, 'file')
    prevdiv = [prevfdts.STBE, prevfdts.STBU, prevfdts.LTBE, prevfdts.LTBU, prevfdts.SENTI, prevfdts.PDIV];
    div = merge(prevdiv, pDIV, 'SortColumns', false);
else
    div = pDIV;
end;

window = 5;
if exist(figdata, 'file')
    mini = 1;
else
    mini = window;
end;
for i = [mini : duration]
    buy(i) = buysignal(div(end - duration + i - window + 1 : end - duration + i));
    sell(i) = sellsignal(div(end - duration + i - window + 1 : end - duration + i));
end;
pBS = fints(pBOLL.dates, [buy', sell'], {'BUY', 'SELL'});

fdts = [pBS, pDIV, pMOV, pBOLL, pTT.TRTDVOL, pWADV.WADVOL];
if exist(figdata, 'file')
    fdts = merge(fdts, prevfdts, 'SortColumns', false);
end;

date = getfield(fdts, 'dates');
fdmat = fts2mat(fdts);
len = length(fdts);

fp = fopen(figdata, 'w');
fprintf(fp, '%s\n', stock);
fprintf(fp, 'DATE\tBUY\tSELL\tSTBE\tSTBU\tLTBE\tLTBU\tSENTI\tPDIV\tMOV200\tMOV100\tMOV50\tMIDDLE\tUPPER\tLOWER\tTRTDVOL\tWADVOL\n');
for i = [len : -1 : 1]
    fprintf(fp, '%s\t%0.1f\t%0.1f\t%0.8f\t%0.8f\t%0.8f\t%0.8f\t%0.8f\t%0.8f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.3f\t%0.8e\t%0.8e\n', datestr(date(i), 23), fdmat(i, :));
end;
fclose(fp);
