function detectrange(varargin)
% detectrange(stock, period)
% period: day or d, week or w, month or m, quarter or q

argsz = size(varargin, 2);
stock = upper('spy');
period = 5;

if argsz >= 1
    stock = upper(varargin{1});
end;
if argsz >= 2
    if varargin{2} == 'day' | varargin{2} == 'd'
        period = 1;
    elseif varargin{2} == 'week' | varargin{2} == 'w'
        period = 5;
    elseif varargin{2} == 'month' | varargin{2} == 'm'
        period = 21;
    elseif varargin{2} == 'quarter' | varargin{2} == 'q'
        period = 63;
    end;
end;

stockdata = ['data/matlab/', stock, '.dat'];
ts = ascii2fts(stockdata, 1, 2, []);
if period == 1
    pts = ts(end - 4 : end);
else
    pts = toperiod(ts, period, 5);
end;

hts = getfield(pts, 'HIGH');
lts = getfield(pts, 'LOW');
range = hts - lts;
[min(range), median(range), max(range)]

function pts = toperiod(ts, period, num)
len = length(ts);
start = len - period * num + 1;
if start <= 0
    start = len - floor(len / period) * period + 1
end;
i = 1;
while start <= len
    high = max(getfield(ts(start : start + period - 1), 'HIGH'));
    low = min(getfield(ts(start : start + period - 1), 'LOW'));
    close = getfield(ts(start + period - 1), 'CLOSE');
    volume = sum(getfield(ts(start : start + period - 1), 'VOLUME'));
    date = getfield(ts(start + period - 1), 'dates');
    pvec(i, :) = [date, close, high, low, volume];
    start = start + period;
    i = i + 1;
end;
pts = fints(pvec(: , 1), pvec(: , 2 : 5), {'CLOSE', 'HIGH', 'LOW', 'VOLUME'});
