#!/bin/bash
# ./searchdetect.sh buy|sell signal
# ./searchdetect.sh buy 3.5
# ./searchdetect.sh sell 4.5

usage() {
  echo "./optionpain stock ::: get max pain of stock"
  echo "./optionpain stock [month [year]] ::: get max pain of stock at month year"
  echo "./optionpain all month year ::: get pain of all, and store in option/year-month"
  echo "./optionpain current stock ::: get current pain of stock againt base"
  echo "./optionpain current all ::: get current pain of all against base"
  echo "./optionpain stat all ::: get pain stat against base, and store in optionstat.txt"
}

searchbuy() {
  cd detect.record
  signal=$1
  for file in * ; do
    start=0
    cat $file | while read line ; do
      if [[ "$line" == *:$'\t'Buy* ]] ; then
        start=1
        continue
      fi
      if [[ "$line" == *ans* ]] ; then
        start=0
        continue
      fi
      if [[ $start == 1 ]] && [ -n "$line" ] ; then
        set $line
        if [[ $3 == $signal ]] ; then
          echo $file $'\t' $line
        fi
      fi
    done
  done
  cd ..
}

searchsell() {
  cd detect.record
  signal=$1
  for file in * ; do
    start=0
    cat $file | while read line ; do
      if [[ "$line" == *:$'\t'Sell* ]] ; then
        start=1
        continue
      fi
      if [[ "$line" == *ans* ]] ; then
        start=0
        continue
      fi
      if [[ $start == 1 ]] && [ -n "$line" ] ; then
        set $line
        if [[ $3 == $signal ]] ; then
          echo $file $'\t' $line
        fi
      fi
    done
  done
  cd ..
}

if [ $# -ne 2 ] ; then
  usage
  eixt 1
fi

if [ $1 = 'buy' ] ; then
  searchbuy $2
elif [ $1 = 'sell' ] ; then
  searchsell $2
else
  usage
  exit 1
fi
exit 0


