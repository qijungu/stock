#!/bin/sh
usage() {
  echo "./rerunstock.sh stock ::: redownload, reanalyze, redraw stock"
  echo "single stock only"
}

if [ $# -ne 1 ]; then
  usage
  exit 1
fi

stock=$(echo $1 | tr [:lower:] [:upper:])
startday=252
endday=0

rm -rf data/${stock}.dat data/matlab/${stock}.dat fig/data/${stock}.dat

mv stock.list stock.list.bak
echo $stock > stock.list

./download stock.list
./runanalyze.sh /opt/matlab ${stock}
./rundrawchart.sh /opt/matlab ${stock} 1 ${startday} ${endday}

rm -rf stock.list
mv stock.list.bak stock.list
