## I am not responsible for your loss or gain.

## Requirements (what I have in my own computer)

+ Fedora 23

+ Matlab R2017a  
  Matlab compiler  
  Matlab toolboxes: financial, financial instruments, neural network, optimization, parallel computing, statistics

+ g++

+ Python 2.7, beautifulsoup4, pytrademonster, requests, lxml

+ jdk1.8.x

## Installation

1. If you installed Matlab to /opt/MATLAB/R2017a, then set the environment variable **MATLAB=/opt/MATLAB/R2017a**.

   Go to /opt/MATLAB/R2017a/sys/os/glnxa64, run "ln -sf /lib64/libstdc++.so.6 ." (in R2017a, resolve the problem of ploting)

   Go to /opt/MATLAB/R2017a/bin/glnxa64, run "ln -sf /lib64/libfreetype.so.6 ."

2. In a directory, **git clone https://gitlab.com/qijungu/stock.git**. You will have a new stock directory with code inside.

3. **chmod ugo+w ${MATLAB}/toolbox/local/pathdef.m** (writable to everyone)

4. Assume your stock directory is "/home/user/stock". Then, run matlab, click the "set path" button, and add "/home/user/stock/code"

5. In the stock directory, **make**

## File organization

+ Code is in stock/ and stock/code.

+ Stock data is in stock/data and stock/data/matlab.  
  Data is from Yahoo finance and Chicago Board Options Exchange (CBOE).

+ Stock figures are in stock/fig and stock/fig/data.

+ You can manually edit the files of any stock to correct some occasional issues.

+ You can remove the files of any stock, and then rerun make or make daily to reset all computed data for that stock.

## Stock, after installation

+ I mostly read VIX.eps, SPX.eps, XIV-nnet.eps, XIV-p.eps, SPY-p.eps  
  I also read all charts from http://stockcharts.com/public/1684859/chartbook  
  Then, I make decisions.

+ To get daily data and charts: **make**  
  Do not do this during 3pm-8pm Central time.  
  Before 3pm, you will only get daily data of yesterday and earlier.  
  After 8pm, you will get daily data of today and earlier.

+ To get real-time data and charts: **make daily**  
  ONLY do this during 9am-3pm Central time.

+ Edit stock.list.part and index.list.part to have your favorite stock and index ticks. This file is used with "make".

+ Edit stock.list.daily and index.list.daily to have your favorite stock and index ticks. This file is used with "make daily".

+ Any stock MUST have at least **two years of data**.

+ To draw stock chart with a specified day range, **./drawstock stock [startday [endday]]**  
  startday is the number of days before today, and endday is the number of days before today.  
  Without startday and endday, it draws chart of one year (252 trading days).  
  
  The following commands are equivalent:  
  ./drawstock stock  
  ./drawstock stock 252  
  ./drawstock stock 252 0  
  
  To see the chart of a year earlier:  
  ./drawstock stock 504 252

+ To draw index chart with a specified day range, **./drawidx index [startday [endday]]**

## Option (monthly only), after installation

+ Option data is from trademonster/optionshouse. So, you will need to have an account there before using this option tool.  
  You must install pytrademonster, "pip install pytrademonster"  
  The first time you run the tool, you will be asked to provide account info to login. After that, you will not be asked again.  
  Your account info is stored in "cred.dat" in your own computer.

+ To get max pain, run  
  ./optionpain stock      ::: get max pain of stock  
  ./optionpain stock [month [year]]      ::: get max pain of stock at month year  
  ./optionpain all month year      ::: get pain of all, and store in option/year-month

+ To get base pain, run **ALWAYS around 4pm-7pm Central time on the third Friday of every month**  
  ./optionpain current all      ::: get current pain of all against base  

+ To get current pain, run  
  **You must have base pain monthly before you calculate current pain**  
  ./optionpain current stock      ::: get current pain of stock againt base

## Common errors

+ When "make" or "make daily", you might see the error below. It is because the data downloaded for a particular stock is not correct. You can check the stock's data in stock/data and stock/data/matlab to see which fields are not correct. The error is usually caused by the data source (Yahoo or CBOE). Just wait until they provide data as normal. If the data error persists, manually correct the stock's data file after three days.

  == error message ==  
  Error using ascii2fts  
  Either the data in the text file is incorrect, or file  
  contents do not correspond to the input information.  
  The actual error generated when creating the FINTS object is:
