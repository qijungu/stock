SRCDIR = code
DEBUG = -Wall -g -O0 -gdwarf-2
RELEASE = -Wall -O3
IOREDIR = 1>/dev/null

MTGTS = detect analyzestock analyzemf analyzeindex drawchart drawmf drawindex detectrange predicttrend
CTGTS = purge analyze drawstock drawfund drawidx stockrange optionstat

all: update
	cp -f stock.list.part stock.list
	cp -f index.list.part index.list
	python ./code/download.py index.list stock.list
	./analyze index.list stock.list
	./drawidx all
	./drawstock all

daily: update
	cp -f stock.list.daily stock.list
	cp -f index.list.daily index.list
	python ./code/downloaddaily.py index.list stock.list
	./analyze index.list stock.list
	./drawidx all
	./drawstock all

vix: update
	cp -f stock.list.vix stock.list
	cp -f index.list.vix index.list
	python ./code/downloaddaily.py index.list stock.list
	./analyze index.list stock.list
	./drawidx all
	./drawstock all

draw:
	cp -f stock.list.vix stock.list
	cp -f index.list.vix index.list
	./analyze index.list stock.list
	./drawidx all
	./drawstock all

complete: update
	cp -f stock.list.comp stock.list
	cp -f index.list.comp index.list
#	./download index.list stock.list
	python ./code/download.py index.list stock.list
	./purge index.list stock.list
	./analyze index.list stock.list
#	./rundetect.sh /opt/matlab
#	./drawidx all
#	./drawstock all
#	./drawfund all
#	cat detect.record/detect-`date '+%Y-%m-%d'`.txt

stock: update
#	./download stock.list
	python ./code/download.py stock.list
	./analyze stock.list
	./drawstock all

help:
	@cat README.txt

updateall: update
	./download index.list stock.list
#	./purge index.list stock.list
#	./analyze index.list stock.list

detectall: update
	@./rundetect.sh /opt/matlab
	@cat detect.record/detect-`date '+%Y-%m-%d'`.txt

drawall: update
	@./analyze index.list stock.list
	@./drawidx all
	@./drawstock all
#	@./drawfund all

clean:
	rm -f $(MTGTS) $(CTGTS)

# matlab code

$(MTGTS) : % : $(SRCDIR)/%.m
	@mcc -I $(SRCDIR) -m $<
	@rm -rf run_$@.sh readme.txt *.log

# c code
update: $(CTGTS) detect predicttrend

purge: $(SRCDIR)/purge.cc
	@g++ $(RELEASE) -o purge $(SRCDIR)/purge.cc

analyze: analyzestock analyzemf analyzeindex $(SRCDIR)/analyze.cc
	@g++ $(RELEASE) -o analyze $(SRCDIR)/analyze.cc

drawstock: drawchart $(SRCDIR)/drawstock.cc
	@g++ $(RELEASE) -o drawstock $(SRCDIR)/drawstock.cc

drawfund: drawmf $(SRCDIR)/drawfund.cc
	@g++ $(RELEASE) -o drawfund $(SRCDIR)/drawfund.cc

drawidx: drawindex $(SRCDIR)/drawidx.cc
	@g++ $(RELEASE) -o drawidx $(SRCDIR)/drawidx.cc

stockrange: detectrange $(SRCDIR)/stockrange.cc
	@g++ $(RELEASE) -o stockrange $(SRCDIR)/stockrange.cc

optionstat: $(SRCDIR)/optionstat.cc
	@g++ $(RELEASE) -o optionstat $(SRCDIR)/optionstat.cc

