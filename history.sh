#!/bin/sh

p=pattern/spy-vix

drwidx() {
./drawidx ${sym} 249 0
mv fig/${sym}.eps ${p}/2012-${sym}.eps
./drawidx ${sym} 501 251
mv fig/${sym}.eps ${p}/2011-${sym}.eps
./drawidx ${sym} 753 503
mv fig/${sym}.eps ${p}/2010-${sym}.eps
./drawidx ${sym} 1005 755
mv fig/${sym}.eps ${p}/2009-${sym}.eps
./drawidx ${sym} 1257 1007
mv fig/${sym}.eps ${p}/2008-${sym}.eps
./drawidx ${sym} 1509 1260
mv fig/${sym}.eps ${p}/2007-${sym}.eps
./drawidx ${sym} 1760 1510
mv fig/${sym}.eps ${p}/2006-${sym}.eps
./drawidx ${sym} 2012 1762
mv fig/${sym}.eps ${p}/2005-${sym}.eps
./drawidx ${sym} 2264 2014
mv fig/${sym}.eps ${p}/2004-${sym}.eps
./drawidx ${sym} 2516 2266
mv fig/${sym}.eps ${p}/2003-${sym}.eps
}

drwstk() {
./drawstock ${sym} 249 0
mv fig/${sym}-p.eps ${p}/2012-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2012-${sym}-t.eps
./drawstock ${sym} 501 251
mv fig/${sym}-p.eps ${p}/2011-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2011-${sym}-t.eps
./drawstock ${sym} 753 503
mv fig/${sym}-p.eps ${p}/2010-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2010-${sym}-t.eps
./drawstock ${sym} 1005 755
mv fig/${sym}-p.eps ${p}/2009-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2009-${sym}-t.eps
./drawstock ${sym} 1257 1007
mv fig/${sym}-p.eps ${p}/2008-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2008-${sym}-t.eps
./drawstock ${sym} 1509 1260
mv fig/${sym}-p.eps ${p}/2007-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2007-${sym}-t.eps
./drawstock ${sym} 1760 1510
mv fig/${sym}-p.eps ${p}/2006-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2006-${sym}-t.eps
./drawstock ${sym} 2012 1762
mv fig/${sym}-p.eps ${p}/2005-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2005-${sym}-t.eps
./drawstock ${sym} 2264 2014
mv fig/${sym}-p.eps ${p}/2004-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2004-${sym}-t.eps
./drawstock ${sym} 2516 2266
mv fig/${sym}-p.eps ${p}/2003-${sym}-p.eps
mv fig/${sym}-t.eps ${p}/2003-${sym}-t.eps
}

cvtidx() {
for y in 2012 2011 2010 2009 2008 2007 2006 2005 2004 2003 ; do
  f=${p}/${y}-${sym}
  if [ -f "${f}".eps ]; then
    epstopdf ${f}.eps
  fi
done
}

cvtstk() {
for y in 2012 2011 2010 2009 2008 2007 2006 2005 2004 2003 ; do
  f=${p}/${y}-${sym}
  if [ -f "${f}-p.eps" ]; then
    epstopdf ${f}-p.eps
    mv ${f}-p.pdf ${f}.pdf
  fi
done
}

for sym in VIX SPX ; do
#  drwidx
  cvtidx
done

for sym in XIV SPY IWM TLT UUP ; do
#  drwstk
  cvtstk
done

for y in 2012 2011 2010 2009 2008 2007 2006 2005 2004 2003 ; do
  for sym in SPX XIV SPY IWM TLT UUP ; do
    if [ -f ${p}/${y}-${sym}.pdf ]; then
      pdfunite ${p}/${y}-VIX.pdf ${p}/${y}-${sym}.pdf ${p}/${y}.pdf
      rm -f ${p}/${y}-VIX.pdf ${p}/${y}-${sym}.pdf
      mv ${p}/${y}.pdf ${p}/${y}-VIX.pdf
    fi
  done
  mv ${p}/${y}-VIX.pdf ${p}/${y}.pdf
done

